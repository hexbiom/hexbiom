# dockerant - A hopefully minimal docker image with apache ant 
# (ant version is 1.10.7, same as in the CIP-Pools at uni Goettingen). 

FROM alpine
MAINTAINER Benjamin Eckhardt <benjamin.eckhardt@stud.uni-goettingen.de>
# inspired by https://github.com/paulushcgcj/apacheant/blob/master/Dockerfile

# java11: 
RUN apk add openjdk11

# ant: mkdir, download, unpack, clean up, set ant home, add to path
RUN mkdir -p /opt/ant/
RUN wget https://archive.apache.org/dist/ant/binaries/apache-ant-1.10.7-bin.tar.xz -P /opt/ant
RUN tar -xf /opt/ant/apache-ant-1.10.7-bin.tar.xz -C /opt/ant/
RUN rm -f /opt/ant/apache-ant-1.10.7-bin.tar.xz
ENV ANT_HOME=/opt/ant/apache-ant-1.10.7
ENV PATH="${PATH}:${ANT_HOME}/bin"

