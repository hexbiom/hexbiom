package doerfer.hexbiom.config;

/**A class for syntactic errors os a config file.  */
public class ConfigFileParseError extends Exception {
    /** The line in which the error occured. */
    public final int line;
    /** What token was excpected, and what was read. */
    public final String exp, got;
    /** The file in which the error occured. */
    public final String file;
    /** A more precise reason what exactly did not work. */
    public final Exception why;
    /** Just sets the fields name to the value of paramter _name.  */
    public ConfigFileParseError(String _file, int _line, String _exp, String _got, Exception _why) {
        line = _line; exp = _exp; got = _got; file = _file; why = _why;
    }
    /** A helpful parse error message. */
    @Override
    public String getMessage() {
        return "Error in Config File  "+file+"  line  "+line+"!  \nEXPECTED  "+exp+"\nGOT       "+got+"\nBECAUSE   "+why.getClass().getSimpleName()+": "+why.getMessage();
    }
}
