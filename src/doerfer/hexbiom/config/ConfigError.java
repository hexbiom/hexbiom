
package doerfer.hexbiom.config;

/** A class for logical errors of a config file + cmd arguments. */
public class ConfigError extends Exception {
    public ConfigError(String msg) {
        super(msg);
    }
}





