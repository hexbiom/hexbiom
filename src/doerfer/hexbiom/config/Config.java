package doerfer.hexbiom.config;

import java.io.*;
import java.util.*;

import org.apache.commons.cli.*;

import doerfer.preset.*;

import doerfer.hexbiom.*;
import doerfer.hexbiom.board.*;


/** Unifies the settings by commandline (Settings / ArgumentParser) and GameConfiguration. (#FP means that something uses an ideom that is common in functional programming.)*/
public class Config extends ArgumentParser implements GameConfiguration {

    /** Save what the corresponding {@link GameConfiguration}-getter is supposed to return. */
    private String description;
    /** Save what the corresponding {@link GameConfiguration}-getter is supposed to return. */
    private int numPlayers, numTiles;
    /** Save what the corresponding {@link GameConfiguration}-getter is supposed to return. */
    private Map<Biome, Integer> biomeChances, biomeWeights;
    /** Save what the corresponding {@link GameConfiguration}-getter is supposed to return. */
    private List<Tile> preplacedTiles;
    /** Save what the corresponding {@link GameConfiguration}-getter is supposed to return. */
    private List<Integer> preplacedPlayerIDs;
    /** Save what the corresponding {@link GameConfiguration}-getter is supposed to return. */
    private List<TilePlacement> preplacedTilesPlacements;

    // For benis fancy statefull parser api.
    /** The config file which will be read incrementally. */
    private BufferedReader file;
    /** Track the current position in file (just for diagn). */
    private Integer lNr = 0;


    /** Parse command line args and settings file s.t. there is a complete configuration to start the game. Raises on failure.
     *  @throws IOException if during parsing an error ocurred reading the file.
     *  @throws ConfigFileParseError describes a problem with the syntax of the config file.
     *  @throws ParseException for an error in the command line args
     *  @throws ConfigError describing a semantically invalid configuration (includes if provided config file does not exist).
     */ public Config(String[] args) throws IOException, ConfigFileParseError, ParseException, ConfigError {
        // == command line arg parsing == \\

        super(args, Main.NAME, Main.VERSION, Main.AUTHORS, Main.OPTIONAL_FEATURES);  // parse args with doerfer.preset.ArgumentParser

        guard(gameConfigurationFile != null, "No config file - Please provide a config file!");


        // == config file parsing == \\  see https://gitlab.gwdg.de/app/2022ss/doerfer-preset/-/blob/main/ANFORDERUNGEN.md#spielkonfiguration

        try {
            file = new BufferedReader(new FileReader(gameConfigurationFile));
        } catch (FileNotFoundException e){
            guard(false, "No config file found at path "+gameConfigurationFile+ " - Please provide a valid file path!");
        }

        ThrowingFunction<Exception, String,String[]> words = s -> s.split("(\\s*,\\s*)|(\\s+)");  // Extract words separated by any spaces and/or a comma
        ThrowingFunction<Exception, String, Integer> _int  = Integer::parseInt;            // #FP Just a shorthand.

        // The info-String should be descriptive. The last arg (test) should be familiar from ANFORDERUNGEN.
        parse(MAGICNUMBER+"  (Magic Number)",                    x -> x.trim(), x -> x.equals(MAGICNUMBER));
        description = parse("String: Description"                       , x->x, x -> true                 );
        numPlayers  = parse("Integer: num Players (> 0)"                , _int, x -> x > 0                );
        numTiles    = parse("Integer: num Tiles on stack (> numPlayers)", _int, x -> x > numPlayers       );


        // Six lines of biome chances weights...
        biomeChances = new HashMap<>(6,1);
        biomeWeights = new HashMap<>(6,1);

        try {   // see catch

        nmal(6, i -> parse("3 words: BIOME CHANCE(int>=0<=100) WEIGHT(int>=0), with exactly one entry per predefined Biome", chain(words, l ->
            new Trip<>(Biome.valueOf(l[0]), _int.apply(l[1]), _int.apply(l[2]))
        ), ws -> ws.b >= 0 && ws.c >= 0 && ws.b <= 100, ws -> {
            biomeChances.put(ws.a, ws.b);           // With this mechanism we handle any error in put the same way as any parse error purely.
            biomeWeights.put(ws.a, ws.c);           // I think this all together exactly captures the space of valid configs and is cool.
        }));


        // Arbitrary many preplaced tiles...
        Integer nPreplaced = parse("number of preplaced tiles (int>=nPlayers)", _int, x -> x >= numPlayers);

        preplacedPlayerIDs       = new ArrayList<Integer      >(nPreplaced);
        preplacedTiles           = new ArrayList<Tile         >(nPreplaced);
        preplacedTilesPlacements = new ArrayList<TilePlacement>(nPreplaced);
        nmal(nPreplaced, l ->
            parse("a preplaced tile as <SPIELERID> <REIHE> <ZEILE> <DREHUNG> <BIOM>,<BIOM>,<BIOM>,<BIOM>,<BIOM>,<BIOM>",
                chain(words, ws -> {
                    int[] is = new int[4];    // Streams are fancier (this 5 lines in 2) but wont accept throwing funtions
                    List<Biome> bs = new ArrayList<Biome>(6);
                    for (int i = 0; i <  4; i++) is[i] = _int.apply(ws[i]);
                    for (int i = 4; i < 10; i++) bs.add(Biome.valueOf(ws[i]));
                    return new Pair<>(is, bs);      // Pair.a are the 4 ints, b the 6 biomes
            }), x -> x.a[0] >= 0 && x.a[0] <= numPlayers, x -> {
                    preplacedPlayerIDs      .add(x.a[0]);
                    preplacedTilesPlacements.add(new TilePlacement(x.a[1], x.a[2], Math.floorMod(x.a[3],6)));
                    preplacedTiles          .add(new BiomeTile(x.b, x.a[0]));
                }
        ));

        } catch (IOException | ConfigFileParseError e) { throw e; }
        catch (Exception e) { } // UNREACHABLE: there can only be the above 2 types, but to capture it in the generics of nmal i would have to write muuch dumb code for a BiThrowingFunction or so. 

        // == verification of semntic integrity (prevents maany bugs) == \\

        // Check if in List v there are at least numPlayers element, else provide a helpful error message using String s.
        ThrowingBiConsumer<ConfigError, List, String> enough = (v, s) ->
            guard(v.size() >= numPlayers, "Not enough "+s+" ("+v.size()+") provided for the "+numPlayers+" players in the config file!");

        enough.on(playerColors, "colors");
        enough.on(playerNames , "names ");
        enough.on(playerTypes , "types ");
    }


    // == parsing == \\

    /** Throw a ConfigError with error description msg if cond is false. */
    private static void guard(boolean cond, String msg) throws ConfigError {
        if (!cond) throw new ConfigError(msg);
    }

    /** #FP Chain two functions.
     * The functions f (A → B) and g (B → C) may throw exceptions X and Y respectively see {@link ThrowingFunction} - there composition a common superclass.
     * @return a function, that applies its argument to f, the result of that to g and returns the result thereof (function compostation reversed).
     */
    public static <A,B,C, E extends Throwable, X extends E, Y extends E> ThrowingFunction<E, A,C>
        chain(ThrowingFunction<X, A,B> f, ThrowingFunction<Y, B,C> g) { return x -> g.apply(f.apply(x)); }

    /** pars: String → R (intermediate representaion). If test(R) ist false, throw. Postprocess with after: R → S. */

    /**
     * #FP Parse a String into a S with checks. Rethrows all errors as ParseError(info).
     * @param <R> an intermediate representation produced by pars, consumed by test and after.
     * @param <S> the final result type.
     * @param info type of thing to parse - used to generate error messages in {@link ConfigFileParseError}.
     * @param pars first applied function: Turn a String into an R.
     * @param test second applied function: Test if the R holds some conditions - if not a respective {@link ConfigFileParseError} is thrown.
     * @param after last applied function: postprocess the validated R into an S.
     * @return a parsed and validated result of type S.
     * @throws ConfigFileParseError on a syntax error.
     * @throws IOException if the file was modified on disk during read.
     */
    private <R, S> S parse(String info, ThrowingFunction<Exception, String, R> pars, ThrowingFunction<Exception, R, Boolean> test, ThrowingFunction<Exception, R, S> after)
    throws ConfigFileParseError, IOException {
        lNr++;
        String line = file.readLine();
        try {
            var res = pars.apply(line);
            if (!test.apply(res))
                throw new Exception("Unmatched constrains.");
            return after.apply(res);
        } catch (Exception e) {
            throw new ConfigFileParseError(gameConfigurationFile.getName(), lNr, info, line, e);
        }
    }

    /** #FP Wrap {@link Config#parse} s.t. after may return void. */
    private <R> void parse(String info, ThrowingFunction<Exception, String, R> pars, ThrowingFunction<Exception, R, Boolean> test, ThrowingConsumer<Exception, R> after)
    throws ConfigFileParseError, IOException {    parse(info, pars, test, r -> {after.accept(r); return null;} );    }

    /** #FP Wrap {@link Config#parse} s.t. after is optional. (Same as {@code after = x->x} (the identity).) */
    private <S> S parse(String info, ThrowingFunction<Exception, String, S> pars, ThrowingFunction<Exception, S, Boolean> test)
    throws ConfigFileParseError, IOException {    return parse(info, pars, test, r->r);    }

    /** #FP Call the void-Function f for every int in [0,n) without collecting results. Wraps any {@link ConfigFileParseError} appripriately, other exceptions are rethrown. */
    private void nmal(Integer n, ThrowingConsumer<Exception, Integer> f)
    throws ConfigFileParseError, Exception {
        Integer i = 0;
        try { for (; i < n; i++) f.accept(i);
        } catch (Exception _e) {
            if (_e.getClass() == ConfigFileParseError.class) {
                var e = (ConfigFileParseError) _e;
                throw new ConfigFileParseError(gameConfigurationFile.getName(), e.line, n+" times: "+e.exp, (i+1)+". time:  "+e.got, e.why);
            }
            throw _e;
        }
    }

    /** Get the number of the line in the configuration file, the config file parser read last. */
    public int getLNr() { return lNr; }


    // == simple getters as per GameConfiguration-Interface == \\

    @Override public String getDescription() { return description; }
    @Override public int getNumPlayers() { return numPlayers; }
    @Override public int getNumTiles() { return numTiles; }
    @Override public Map<Biome, Integer> getBiomeChances() { return biomeChances; }
    @Override public Map<Biome, Integer> getBiomeWeights() { return biomeWeights; }
    @Override public List<Tile> getPreplacedTiles() { return preplacedTiles; }
    @Override public List<Integer> getPreplacedTilesPlayerIDs() { return preplacedPlayerIDs; }
    @Override public List<TilePlacement> getPreplacedTilesPlacements() { return preplacedTilesPlacements; }
}


// == utility stuff that is built-in in haskell: tuples (typsafe heterogenous collections)  == \\

/** #FP Very simple binary generic tuple. Access fields alphabetically. */
class Pair<A,B> {
    public A a; public B b;
    public Pair(A _a, B _b) { a=_a; b=_b; }
}

/** #FP Very simple ternary generic tuple. Access fields alphabetically. */
class Trip<A,B,C> {
    public A a; public B b; public C c;
    public Trip(A _a, B _b, C _c) { a=_a; b=_b; c=_c; }
}


/** #FP Trivial throwing versions of some well known functional interfaces. Specify the type of thrown excptions as the first parameter.
 *  inspired by <a href="https://stackoverflow.com/questions/18198176/java-8-lambda-function-that-throws-exception">...</a> */
@FunctionalInterface interface ThrowingConsumer  <X extends Throwable, A>   { void accept(A a)  throws X; }    /** see ThrowingConsumer */
@FunctionalInterface interface ThrowingFunction  <X extends Throwable, A,B> { B    apply (A a)  throws X; }    /** see ThrowingConsumer */
@FunctionalInterface interface ThrowingBiConsumer<X extends Throwable, A,B> { void on(A a, B b) throws X; }
