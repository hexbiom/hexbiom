package doerfer.hexbiom.score;

import doerfer.hexbiom.board.BiomeTile;
import doerfer.hexbiom.board.Board;
import doerfer.hexbiom.board.Coords;

import java.util.*;

import static java.lang.Math.floor;
import static java.lang.Math.pow;

/**
 * this class collects all data and methods needed to calculate the score for a given board at a certain state
 * use the constructor {@link Score#Score} to create all needed data from a board
 * then use {@link Score#getAllScores(int)} to retrieve the scores
 * @see BiomeCoords
 * @see BiomeTileComponent
 * @see BiomeField
 * @see Board
 */
public class Score {

    /**
     * hashmap provides functionality for Key,Value pairs with unique pairs
     * this way it is secured that never two components are stored on the same coordinate
     */
    private final Map<BiomeCoords, BiomeTileComponent> biomeGrid = new HashMap<>();

    /**
     * open Edges in a Biomefield result in "incompleted" and are therefore important to store
     */
    private final List<BiomeCoords> openEdges = new ArrayList<>();

    // 0, 1, 2, 3, 4, 5
    // clockwise starting north
    private final int[][] nextEdgeInDirection = {{0,-1, +1}, {+1, -1, 0}, {+1, 0, -1}, {0, +1, -1}, {-1, +1, 0}, {-1, 0, +1}};

    /**
     * constructs a score object for a given state of a board
     * <p>if the board is altered a new score object needs to be constructed</p>
     * @param board the board scoring is wanted for
     */
    public Score(Board board) {

        for(Map.Entry<Coords, BiomeTile> entry : board.getGrid().entrySet()){
            if (entry.getValue() == null) continue;
            BiomeTile tile = entry.getValue();
            // alle edges werden zu eindeutigen koordinaten (mit den bisherigen coords)
            // die Teile der Tiles werden zu Values der neuen Map, playerID wird auch darin gespeichert
            // ziemlich analog zur Board-Klasse
            List<BiomeCoords> edgesOfCenter = new ArrayList<>();
            for (int i = 0; i < 6; i++) {
                BiomeCoords bc = new BiomeCoords(entry.getKey(), i);
                BiomeTileComponent edgeC = new BiomeTileEdge(tile.getEdge(i), tile.getPlayerID());
                biomeGrid.put(bc, edgeC);

                edgesOfCenter.add(bc);
            }

            BiomeTileCenter centerC = new BiomeTileCenter(tile.getCenter(), tile.getPlayerID());
            centerC.setNeighbours(edgesOfCenter); //alle anliegenden Kanten hinzufügen

            biomeGrid.put(new BiomeCoords(entry.getKey(), 6), centerC);
        }


        // nicht center kriegen Verknüpfung für äußere Komponente
        for (BiomeCoords key : biomeGrid.keySet()){
            if (biomeGrid.get(key) instanceof BiomeTileEdge) {
                // if edge füge ref. auf außen anliegende Kante hinzu
                ((BiomeTileEdge) biomeGrid.get(key)).setOuterNeighbour(diffEdge(key));
                if (diffEdge(key) == null) {
                    openEdges.add(key);
                }
                // und innen anliegende Kanten +
                ((BiomeTileEdge) biomeGrid.get(key)).setInnerNeighbours(neighboursOf(key));
            }
        }

    }

    /**
     * returns the neighbours (left, right, center) of the same BiomeTile of a BiomeTileEdge, not a outerEdge
     * @param me the Coords the neighbours for are wanted
     * @return a List of coords for the neighbours
     * @see Score#diffEdge(BiomeCoords)
     */
    private List<BiomeCoords> neighboursOf(BiomeCoords me){
        List<BiomeCoords> neighboursOfEdge = new ArrayList<>();

        // neighbour right
        BiomeCoords keyRight = searchKey(me.getQ(), me.getR(), me.getS(), (me.getB()+1)%6);
        neighboursOfEdge.add(keyRight);

        // neighbour left
        BiomeCoords keyLeft = searchKey(me.getQ(), me.getR(), me.getS(), Math.abs(me.getB()-1)%6);
        neighboursOfEdge.add(keyLeft);

        // neighbour center
        BiomeCoords keyCenter = searchKey(me.getQ(), me.getR(), me.getS(), 6);
        neighboursOfEdge.add(keyCenter);

        return neighboursOfEdge;
    }

    /**
     * determine the different BiomeTile neighboured edge
     * @param me the tile to find the outside neighbour of
     * @return null if no outside adjacent tile, else the reference to this BiomeTileComponent
     */
    private BiomeCoords diffEdge(BiomeCoords me) {
        return searchKey(me.getQ() + nextEdgeInDirection[me.getB()][0],
                me.getR() + nextEdgeInDirection[me.getB()][1],
                me.getS() + nextEdgeInDirection[me.getB()][2],
                (me.getB() + 3) % 6);
    }


    /**
     * all Scores for all players are calculated and returned as list
     * @param numberOfPlayers the amount of players taking part in the game
     * @return a map of the scores of all players from index 1 to number of player, index 0 is contained and always 0 (no player 0)
     */
    public HashMap<Integer, Integer> getAllScores(int numberOfPlayers){
        HashMap<Integer, Integer> allScores = new HashMap<>();
        for (int i = 1; i <= numberOfPlayers+1; i++) {
            allScores.put(i, 0);
        }

        for(Map.Entry<BiomeCoords, BiomeTileComponent> entry : biomeGrid.entrySet()){
            if(entry.getValue() != null){
                BiomeField field = new BiomeField();
                fieldBuilder(field, entry.getKey());
                if(!field.isIncomplete()){

                    int points = scoreFormula(field.getMembers().size(), field.getSize());
                    for (int member:
                            field.getMembers()) {
                        if (member != 0){
                            allScores.put(member, (allScores.get(member) + points));
                        }
                    }
                }
            }
        }
        return allScores;
    }

    /**
     * for a given field and starting component, all parts of the whole are determined with iterative conditioned recursion
     * <p>side effects are: <ul>
     *     <li>size of field is increased if outer edge is matching</li>
     *     <li>owners of tileComponents are tracked and stored in the field</li>
     *     <li>field is set incomplete if any edge traversed to is null or on list of {@link Score#openEdges}</li> </ul> </p>
     * @param field the field which parts are to be determined
     * @param currentCoords the starting component
     */
    private void fieldBuilder(BiomeField field, BiomeCoords currentCoords) {
        if (currentCoords == null || openEdges.contains(currentCoords)) {
            field.setIncomplete();
            return;
        }

        BiomeTileComponent current = biomeGrid.get(currentCoords);
        current.visit(); //avoids walking same path twice
        field.addMember(current.getPlayerID()); // for score formula


        //center
        if (current instanceof BiomeTileCenter) {
            BiomeTileCenter center = (BiomeTileCenter) current;
            List<BiomeCoords> matchingN = keepMatching(currentCoords, center.getNeighbours());

            for (BiomeCoords match: matchingN) {
                // recursive call
                if (biomeGrid.get(match).isUnvisited()) fieldBuilder(field, match);
            }
        }
        //edge
        if (current instanceof BiomeTileEdge) {
            BiomeTileEdge edge = (BiomeTileEdge) current;
            // outer
            BiomeTileEdge outerN = (BiomeTileEdge) biomeGrid.get(edge.getOuterNeighbour());
            if (outerN.match(edge) && outerN.isUnvisited()) {
                field.increase(); // size of biomefield (-> points)
                // recursive call
                fieldBuilder(field, edge.getOuterNeighbour());
            }
            // same Tile
            List<BiomeCoords> matchingN = keepMatching(currentCoords, edge.getInnerNeighbours());

            for (BiomeCoords match: matchingN) {
                // recursive call
                if (biomeGrid.get(match).isUnvisited()) fieldBuilder(field, match);
            }
        }
    }

    /**
     * trims a given list of BiomeCoords for a given single BiomeCoords, that only matching biome are kept
     * @param checkFor the BiomeCoords of a Component which should be matched with
     * @param unchecked the List of BiomeCoords which should be sorted
     * @return all elements of unchecked which corresponding Component was matching the component of checkFor
     */
    private List<BiomeCoords> keepMatching(BiomeCoords checkFor, List<BiomeCoords> unchecked){
        List<BiomeCoords> matching = new ArrayList<>();
        for (BiomeCoords neighbour : unchecked) {
            if (biomeGrid.get(neighbour).match(biomeGrid.get(checkFor))){
                matching.add(neighbour);
            }
        }
        return matching;
    }
    /**
     * formula for score calculation
     * @param n number of Players taking part in the field
     * @param A size of the field
     * @return result of formula
     */
    private int scoreFormula (int n, int A){
        return (int) ((n*A)+floor(pow(A, 1.5)));
    }

    /**
     * delivers the reference to the {@link BiomeCoords} object in the {@link Score#biomeGrid}, for given coords as integers
     * @param q the column in cube coords
     * @param r the row in cube coords
     * @param s the third coordinate in cube coords
     * @param b the position of a {@link BiomeTileComponent} on the {@link BiomeTile} (0-5 for edges clockwise from north, 6 for center)
     * @return the BiomeCoords object for the coordinates, null if not existing
     */
    private BiomeCoords searchKey (int q, int r , int s, int b){
        for(BiomeCoords key : biomeGrid.keySet()) {
            if(key.getQ()==q
                    && key.getR() == r
                    && key.getS() == s
                    && key.getB() == b){
                return key;
            }
        }
        return null;
    }


}
