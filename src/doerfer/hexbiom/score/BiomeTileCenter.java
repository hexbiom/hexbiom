package doerfer.hexbiom.score;

import doerfer.preset.Biome;

import java.util.LinkedList;
import java.util.List;

/**
 * special case of component
 * with functionality only for center components
 */

public class BiomeTileCenter extends BiomeTileComponent{

    /**
     * the six edges around the center
     */
    private List<BiomeCoords> neighbours = new LinkedList<>();
    public BiomeTileCenter(Biome biome, int playerID) {
        super(biome, playerID);
    }

    public List<BiomeCoords> getNeighbours() {
        return neighbours;
    }

    public void setNeighbours(List<BiomeCoords> neighbours) {
        this.neighbours = neighbours;
    }
}
