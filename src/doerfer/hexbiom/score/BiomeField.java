package doerfer.hexbiom.score;

import java.util.ArrayList;
import java.util.List;

/**
 * stores all data (completion, size, owners) connected to one biomefield
 */
public class BiomeField {

    private final List<Integer> members = new ArrayList<>();

    /*
    default false: any OpenEdge result in incompletion of the field, therefore this is tested and results in setting this true
    checking everything for closedEdge is much more laborious
     */
    private boolean incomplete = false;
    /**
     * the amount of inter tile connections
      */
    private int size = 0;

    /**
     * use this if an outer edge is walked over
     */
    public void increase(){
        size++;
    }

    /**
     * use this on every new part of the field to store all owners of the whole field
     * @param playerID a owner of the field
     */
    public void addMember(int playerID) {
        if(!members.contains(playerID)) members.add(playerID);
    }

    public List<Integer> getMembers() {
        return members;
    }

    public int getSize() {
        return size;
    }

    public void setIncomplete() {
        incomplete = true;
    }

    public boolean isIncomplete() {
        return incomplete;
    }
}
