package doerfer.hexbiom.score;

import doerfer.preset.Biome;

/**
 * every component of a tile is for scoring stored as a BiomeTileComponent
 * functionality which is common for edge and center are stored here
 * @see BiomeTileEdge
 * @see BiomeTileCenter
 */
public class BiomeTileComponent {

    private final Biome biome;
    private final int playerID;
    private boolean unvisited = true;

    public BiomeTileComponent(Biome biome, int playerID) {
        this.biome = biome;
        this.playerID = playerID;
    }

    @Override
    public String toString() {
        return "BiomeTileComponent{" +
                "biome=" + biome +
                ", playerID=" + playerID +
                ", unvisited=" + unvisited +
                '}';
    }

    public boolean isUnvisited() {
        return unvisited;
    }

    /**
     * marks this component as visited. this way using the same path twice is avoided
     */
    public void visit() {
        this.unvisited = false;
    }

    public int getPlayerID() {
        return playerID;
    }

    public Biome getBiome() {
        return biome;
    }

    /**
     * compares Biome of a Component with this' Biome
     * @param that the Component which should be compared with
     * @return true if the Biome is identical, else false
     */
    public boolean match(BiomeTileComponent that){
        return this.biome == that.getBiome();
    }

}
