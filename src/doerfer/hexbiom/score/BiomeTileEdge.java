package doerfer.hexbiom.score;

import doerfer.preset.Biome;

import java.util.ArrayList;
import java.util.List;

/**
 * special case of component
 * with functionality only for edge components
 */

public class BiomeTileEdge extends BiomeTileComponent {

    /**
     * the edge of a different biomeTile next to this edge
     */
    private BiomeCoords outerNeighbour;

    /**
     * the left, right and center neighbours of the same biomeTile
     */
    private List<BiomeCoords> innerNeighbours = new ArrayList<>();

    public BiomeTileEdge(Biome biome, int playerID) {
        super(biome, playerID);
    }

    public BiomeCoords getOuterNeighbour() {
        return outerNeighbour;
    }

    public void setOuterNeighbour(BiomeCoords outerNeighbour) {
        this.outerNeighbour = outerNeighbour;
    }

    public List<BiomeCoords> getInnerNeighbours() {
        return innerNeighbours;
    }

    public void setInnerNeighbours(List<BiomeCoords> innerNeighbours) {
        this.innerNeighbours = innerNeighbours;
    }


}
