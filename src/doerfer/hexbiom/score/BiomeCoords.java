package doerfer.hexbiom.score;

import doerfer.hexbiom.board.*;

import java.util.Objects;

/**
 * equivalent to {@link Coords} for {@link Board}
 * one tile from board contains 7 components, which result in a 4th coordinate b
 * this way it is secure that there are never overlapping coords for components. objects of this class are keys in a hashmap
 */
public class BiomeCoords extends Coords {

    final int b;

    /**
     * Constructor from Coords and the b coordinate which states which component on tile is stores in the BiomeCoords
     * @param qrs the Coords Object for the parent tile, q,r,s are taken from here
     * @param directionOnTile 0-5 for edge (which edge on the tile it is) 6 for center
     */
    public BiomeCoords(Coords qrs, int directionOnTile){
        super(qrs.getQ(), qrs.getR(), qrs.getS());
        b = directionOnTile;
    }

    /**
     * for hashmap unique keys
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        BiomeCoords that = (BiomeCoords) o;
        return b == that.b;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), b);
    }

    public int getB() {
        return b;
    }

    @Override
    public String toString() {
        return super.toString()+"{" +
                "b=" + b +
                '}';
    }
}
