package doerfer.hexbiom;

import java.io.IOException;
import java.util.*;

import org.apache.commons.cli.ParseException;

import doerfer.hexbiom.score.Score;
import doerfer.preset.*;
import doerfer.hexbiom.board.BiomeTile;
import doerfer.hexbiom.config.*;
import doerfer.hexbiom.gui.*;
import doerfer.hexbiom.player.*;


public class Main {

    public static final String NAME = "HEXBiOM";
    public static final String VERSION = "0.1.0";
    public static final List<String> AUTHORS = Arrays.asList(
        "Janik Hartger",
        "Moritz Kuschel",
        "Benjamin Eckhardt"
    );
    public static final List<OptionalFeature> OPTIONAL_FEATURES = Arrays.asList(OptionalFeature.ANIMATIONS);

    /** The user {@link Interface} to interact with the user. */
    private static Interface gui;

    /** The {@link TileGenerator} to generate new tiles out of seeds. */
    private static TileGenerator tileGen;
    /** The players to let play together. */
    private static List<Player> players;
    /** The games initial configuration. */
    private static Config conf;

    /** information only relevant for error display */
    private static Map<Player, String> names;
    /** information only relevant for error display */
    private static Map<Player, PlayerType> types;

    /** number of tiles left to uncover */
    private static int ntiles;


    /// This main method provides some PROVISIONAL failure handling for main_ were the _real_ stuff happens.
    public static void main(String[] args) {
        names = new HashMap<>(); types = new HashMap<>();
        try {
            conf = new Config(args); // get an object representing a complete and valid game configuration (exits on fail)
            tileGen = new TileGenerator(conf); // prepare a tile generator
        try {
                game(args);
            } catch (GUIException e) {
                System.out.println("A fatal error occured with the graphics assets! The game will terminate now.");
                System.out.println(e.getMessage());
                System.exit(1);
            }
        } catch (ConfigFileParseError | ConfigError | IOException | ParseException e) {
            System.out.println("An error with the config file occured.");
            System.out.println(e.getMessage());
            System.exit(1);
        }
    }

    // NOTE on exception handling: diese methode exited by jeder exception (da die alle game beendent sind. in main werden die aufgefangen und angezeigt)
    /** The game start, loop and end. May exit on a fatal {@link GUIException}. {@link CheatExceptionBy}s are handled through the gui. */
    public static void game(String[] args) throws GUIException {
        // EXCEPTION handling (cannot display in gui)
        gui = new Interface(conf); // generate (visual) frontend

        ntiles = conf.getNumTiles();
        int nturns = conf.getNumTiles();

        try {
        try {

        // CheatException on illegal initialzed (only REMOTE player could complain falscher weise)
        makePlayers();

        for (int i = 0; i < players.size()+1; i++)
            uncover();  // uncover n_players + 1 cards and message around

        gui.initDisplay();

        int noturn = 0;
        boolean ingame = true;

        while (ingame) { // Game loop

            for (var p : players) { // game loop trough players

                // request player p's turn
                TilePlacement turn;
                if (p instanceof Human)  // human is not paused
                    turn = trycheat(() -> p.requestTilePlacement(), p ,"turn request");
                else {
                    Thread.sleep(conf.delay / 2);
                    turn = trycheat(() -> p.requestTilePlacement(), p ,"turn request");
                    Thread.sleep(conf.delay / 2);
                }

                // distribute the turn to display and all players
                gui.notifyTilePlacement(turn);

                for (var q : players)
                    trycheat_(()->q.notifyTilePlacement(turn), p ,"turn by "+types.get(p).toString()+" "+names.get(p));


                nturns--;

                /// this makes the game leave after a full round in which nothing happend or when no turns left.
                noturn = turn==null ? noturn+1 : 0;
                if (noturn > players.size() || nturns <= 0) {
                    ingame = false;
                    break;
                } else if (turn != null && ntiles > 0) {
                    // uncover a new card (and message it around)
                    uncover();
                }
            }
        }

        // verify game
        Score scoreboard = new Score(gui.getBoard());

        List<Long> seeds = getAllSeeds();
        List<Integer> scores = getAllScores(scoreboard.getAllScores(conf.getNumPlayers()));

        for (Player p: players) {
            trycheat_(()->p.verifyGame(seeds, scores), p, "final game verification");
        }

        /// display results
        gui.gameOver(null); // why should we pass a boolean anyway, there is only one ireversible gameover

        } catch (CheatExceptionBy e) {
            gui.gameOver(e);
        }

        } catch (InterruptedException e) { Thread.currentThread().interrupt(); }
    }

    /**
     * Constructs and initializes the players as specified in the {@link Config} conf. Remebers their names and types.
     * @throws CheatExceptionBy in case the initialization was reported as cheaty by a player.
     */
    private static void makePlayers() throws CheatExceptionBy {
        players = new ArrayList<>(conf.getNumPlayers());

        for (int id = 1; id <= conf.getNumPlayers(); id++) {    // remember: 0 is reserved for neutral/preplaced tiles
            Player p;
            String name = conf.playerNames.get(id-1);
            var    type = conf.playerTypes.get(id-1);
            switch (type) {
                case HUMAN:    p = new Human   (name, gui); break;
                case RANDOM_AI:p = new RandomAI(name); break;
                default: throw new UnsupportedOperationException("Unsupported player type  "+conf.playerTypes.get(id)); // UNREACHABLE (handled in Config)
            }
            players.add(p);
            names.put(p, name);
            types.put(p, type);

            final int pid = id; // because java: "local variables bound in lambdas must be final or effectively final" (?!)

            trycheat_(() -> p.init(conf, pid), p, "initialization");
        }
    }

    /** Requests seeds from the players, constructs a new tile out of them, messages it around as the new uncovered and displays it.
     * Updates the number of tiles to uncover ntiles that is used for control flow.
     * @throws CheatExceptionBy If the tile uncover or the previous seed request was reported as cheaty by a player.
     * @throws GUIException If the uncovered tile could not be displayed (due to a probably fatal graphics error due to corrupted assets).
     */
    private static void uncover() throws CheatExceptionBy, GUIException {
        ntiles--;

        long seed = 0;
        for (var p : players)
            seed ^= trycheat(()->p.requestNextRandomNumber(), p, "seed request");

        Tile tile = new BiomeTile(tileGen.generateTile(seed));

        gui.notifyNewUncoveredTile(tile);

        for (var p : players)
            trycheat_(()->p.notifyNewUncoveredTile(tile), p, "tile uncovering");
    }

    /** Wraps the other trycheat so that the function act must not return a value. */
    private static void trycheat_(RunnableExcept<Exception> act, Player player, String situation) throws CheatExceptionBy {
        trycheat(() -> {act.run(); return null;}, player, situation);
    }

    /**
     * Executes act and returns its result, if it fails with an exception (especially an {@link CheatException}) that is wrapped into an {@link CheatExceptionBy}.
     * @param act A runnable that may return a value and throw any {@link Exception}.
     * @param p the player that was on turn / acted uppon when act was called.
     * @param situation a description of the situation in which act was called.
     * @return the result of act if it succeeded.
     * @throws CheatExceptionBy incase of an exception aggregates information of the player who threw it, the reason he possible gave and the situation it which it happened.
     */
    private static <R> R trycheat(SupplierExcept<Exception,R> act, Player p, String situation) throws CheatExceptionBy {
        try {   return act.get();
        } catch (Exception e) {
            String name = names.get(p);     PlayerType type = types.get(p);
            if      (e instanceof CheatExceptionBy)     throw (CheatExceptionBy)e;
            else if (e instanceof CheatException)
                throw new CheatExceptionBy((CheatException)e, name, type, situation);
            // else if (e instanceof NetworkException) // would be for networking (show brosenne we thought about it)
            else {
                throw new CheatExceptionBy(e.getMessage(), name, type, situation);
            }
        }
    }

    /**
     * is used after game over for verification
     * @param scores a map created by {@link Score#getAllScores(int)}
     * @return a list of scores index = playerID, index 0 is 0 (player 0)
     */
    private static List<Integer> getAllScores(Map<Integer, Integer> scores) {
        List<Integer> scoreList = new ArrayList<>();
        scoreList.add(0); // spieler 0 (neutral)

        for (int i = 1; i <= conf.getNumPlayers(); i++) {
            scoreList.add(scores.get(i));
        }
        return scoreList;
    }

    /**
     * is used after game over for verification
     * @return List of seeds for all players
     */
    private static List<Long> getAllSeeds() throws CheatExceptionBy {
        List<Long> seeds = new ArrayList<>();
        for (Player p : players) {
            trycheat_(()->seeds.add(p.requestRandomNumberSeed()), p, "final seed sharing request");
        }
        return seeds;
    }
}

/** Like {@link java.util.function.Supplier} but may throw an exception of type X. */
@FunctionalInterface interface SupplierExcept  <X extends Exception, A>   { A get()  throws X; }
/** Like {@link Runnable} but may throw an exception of type X. */
@FunctionalInterface interface RunnableExcept  <X extends Exception>   { void run()  throws X; }
