package doerfer.hexbiom.gui;

import java.util.*;

import java.io.IOException;

import com.kitfox.svg.*;

import doerfer.preset.*;
import doerfer.preset.graphics.*;

import doerfer.hexbiom.board.*;
import doerfer.hexbiom.config.*;
import doerfer.hexbiom.gui.animation.*;

/**
 * This abstract class stores common functionality of MainLayer and HudLayer. Which is mainly the objects they both want to refer to.
 * @see MainLayer
 * @see HudLayer
 * @see Interface
 */

public abstract class Layer {
    protected Interface root;
    protected GPanel panel;
    protected GGroup group;
    protected Board  board;
    protected Config conf;
    protected Animator renderer;

    protected Layer(Interface root, Animator renderer, GPanel panel, Board board, GGroup group, Config conf) {
        this.root  = root;
        this.panel = panel;
        this.board = board;
        this.group = group;
        this.conf  = conf;
        this.renderer = renderer;
    }
}
