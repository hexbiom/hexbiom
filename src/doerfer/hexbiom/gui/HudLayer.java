package doerfer.hexbiom.gui;

import java.awt.Color;
import java.util.*;
import java.time.*;
import java.time.temporal.ChronoUnit;

import doerfer.preset.*;
import doerfer.preset.graphics.*;

import doerfer.hexbiom.score.*;
import doerfer.hexbiom.board.*;
import doerfer.hexbiom.config.*;
import doerfer.hexbiom.gui.animation.*;

import static doerfer.hexbiom.gui.animation.Sprite.Dim.*;
import static doerfer.hexbiom.gui.animation.Path.Change.*;
import static doerfer.preset.LogLevel.*;
import static java.awt.Color.*;

/** Menages drawing ot the HUD. */
public class HudLayer extends Layer {

    /** Hold the currently uncovered and unplaced cards. */
    private Deck<GTile> cards;
    /** Hold the currently displayed notifications. */
    private Deck<Sprite<GText>> infos;
    /** Hold the entries of the scoreboard. */
    private Deck<ScoreBoardEntry> scores;
    /** Displays the current number of cards that may still be placed. */
    private CardsLeft cardsLeft;

    /** A flip used by {@link Interface} to signal wether we are before the first turn (preliminary uncovering). */
    boolean preGame = true;

    private final GRect scoreBoardBG; // stored here to remove when game over

    /** Start the HUD up with some reference objects to operate with. */
    public HudLayer(Interface root, Animator renderer, GPanel panel, Board board, Config conf)  {
        super(root, renderer, panel, board, panel.getLayerHUD(), conf);

        cards  = new Deck<>(conf.getNumPlayers()+1);
        infos  = new Deck<>(3);

        // just a background for the scoreboard
        scoreBoardBG = new GRect(1180, 105, 10000, 80+ conf.getNumPlayers()*50);
        scoreBoardBG.setFill(BLACK);
        scoreBoardBG.setFillOpacity(.3f);
        scoreBoardBG.setCornerRadius(10, 10);
        scoreBoardBG.setStroke(BLACK);
        group.addChild(scoreBoardBG);

        cardsLeft = new CardsLeft(1200, 150, conf.getNumTiles());

        scores = new Deck<>(conf.getNumPlayers());
        Score scorer = new Score(board);
        Map<Integer, Integer> currentscores = scorer.getAllScores(conf.getNumPlayers());
        for (int p = 0; p < conf.getNumPlayers(); p++) {
            var entry = new ScoreBoardEntry(1200, 210+50*p, conf.playerNames.get(p), conf.playerColors.get(p), p+1);
            entry.updateScore(currentscores.get(p+1));
            scores.next(entry);
        }
        scores.top().activate();

        message(" ~~~ Welcome to HEXBIOM - an award aspiring doerfer implementation! ~~~   —", DEBUG);
    }

    /** see message (LogLevel defaults to INFO) */
    public void message(String text) { message(text, INFO); }
    /** Display text as an animated *unobstrusive* message to the user. ll specifies display characteristics. */
    public void message(String text, LogLevel ll) {
        var neu = new Sprite<>(renderer, 0,0, new GText(0, 0,
            (ll == DEBUG ? "           —   " : "["+LocalTime.now().truncatedTo(ChronoUnit.SECONDS)+"]   —   ")+text));

        neu.elem().setFontSize(28);
        neu.elem().setFill(ll == INFO       ? GRAY
                         : ll == WARNINGS   ? ORANGE
                         : ll == ERRORS     ? RED
                         : /* == DEBUG */     YELLOW);  // we use debug for the special welcome message

        neu.start(group, 50, ll == DEBUG ? 50 : 100, 1, 0, 1);

        if (ll == DEBUG) neu.pause(10000);
        if (ll == ERRORS) neu.pause(25000);
        if (ll == INFO) neu.pause(30000);
        double t = 3000;
        neu.to(X, 0, 10000, REL).to(Y, -30, t, ABS).to(O, 0, t, ABS);

        var old = infos.next(neu);
        if (old != null) old.stop();
    }

    /** Update the display of enqued tiles if tile was placed (animated). */
    GTile placeTile(final TilePlacement tp, final BiomeTile tile) {
        if (tp == null) message(scores.top().name.getText()+ "  has been skipped.", INFO);

        scores.top().deactivate();
        scores.shift(1);
        scores.top().activate();

        if (tp != null && tile != null) {
            GTile placed = cards.next(null);
            remove(placed);
            sift();

            cardsLeft.decr();
            return placed;
        } else return null;
    }


    /** Update the display of enqued tiles to show the new tile (animated). */
    void uncoverTile(final BiomeTile btile) throws GUIException {
        var tile = new GTile(renderer, btile, panel);

        tile.start(group, -100, 1000, 0.1, -600, 1);
        tile.to(100, 800, 1.2, 0, 1, 1000);

        GTile prev;
        if (preGame) prev = cards.next(tile);
        else         prev = cards.set(-1,tile);
        remove(prev);

        if (preGame) sift();
    }

    /** Display an animation of progess in the queue of uncovered cards. */
    private void sift() {
        for (var t : cards)
            if (t != null && t != cards.bottom())
                t.to(Y, -150, 1000, REL);

        if (cards.top() != null)
            cards.top().to(S, 1.7, 1000, ABS);
    }


    /** Remove a tile from display (continues existing, call stop if you want to enable Garbage Collection) */
    private void remove(GTile tile) {
        if (tile != null)
            tile.stop();
    }

    /** Display a rotation animation for the tile to place. */
    public void notifyMouseWheel(int rotation) {
        cards.top().to(R, rotation * 60, 500, ABS);
    }

    public void gameOver(Exception e) {

        // add gameOver background
        var gameOverBG = new GRect(0, 0, 10000, 8000);
        gameOverBG.setFill(BLACK);
        gameOverBG.setFillOpacity(.9f);
        gameOverBG.setStroke(BLACK);
        group.addChild(gameOverBG);

        //remove visible hud elements
        group.removeChild(scoreBoardBG);
        group.removeChild(cardsLeft);

        var endNote = new GText(100, 100, "---");
        endNote.setFontSize(50);
        group.addChild(endNote);

        var endText = new GText(200, 200, "Press X to exit  (game automatically exits after 3 minutes).");
        endText.setFontSize(30);
        group.addChild(endText);
        endText.setFill(WHITE);


        if (e == null){
            //remove scoreboard
            for (var s : scores)
                group.removeChild(s);

            endNote.setFill(WHITE);
            endNote.setText("— The Game ended and every body played by the rules.  —");


            // find winner
            Deck<ScoreBoardEntry> gameOverScores = new Deck<>(conf.getNumPlayers());
            Score scorer = new Score(board);
            Map<Integer, Integer> currentscores = scorer.getAllScores(conf.getNumPlayers());

            // find maximum
            Integer temp = 0;
            for (int i = 1; i <= conf.getNumPlayers(); i++) {
                if (currentscores.get(i) > temp) {
                    temp = currentscores.get(i);
                }
            }

            // add game over scoreboard
            for (int p = 0; p < conf.getNumPlayers(); p++) {
                var entry = new ScoreBoardEntry(500, 350+100*p, conf.playerNames.get(p), conf.playerColors.get(p), p+1);
                gameOverScores.next(entry);
            }
            // add scores and winner text
            for (var s : gameOverScores) {
                if (Objects.equals(currentscores.get(s.playerID), temp))
                    s.scor.setText(""+ currentscores.get(s.playerID)+ "                                                                  WINNER");
                else s.scor.setText(""+currentscores.get(s.playerID));
            }
        } else {
            endNote.setFill(RED);
            endNote.setText("— The Game ended with an error!  —");

            var endError = new GText(200, 400, e.getMessage());
            endError.setFontSize(40);
            endError.setFill(ORANGE);
            group.addChild(endError);
        }
    }

    /** Update the scoreboard. */
    public void updateScores() {
        Score scorer = new Score(board);
        Map<Integer, Integer> currentScores = scorer.getAllScores(conf.getNumPlayers());

        for (ScoreBoardEntry s : scores) {
            s.updateScore(currentScores.get(s.playerID));
        }
    }

    /** Aggregate functionality: set position and visibility on the hud layer. */
    private class HUDElement extends GGroup {
        HUDElement(float x, float y) {
            super(panel.getDiagram());

            group.addChild(this);
            transform().translate(x, y);
        }
    }

    /** Mulitple of these make up a scoreboard. */
    private class ScoreBoardEntry extends HUDElement {
        GText name, scor; int playerID;

        /** Set position and associated players name color and id for its scoreboard entry. */
        ScoreBoardEntry(float x, float y, String pname, Color color, int pid) {
            super(x,y); playerID = pid;

            name = new GText(60,0, pname);      scor = new GText(0,0, "---");
            name.setFill(color);                scor.setFill(color);

            addChild(name);                     addChild(scor);
            name.update();                      scor.update();
        }

        /** Show a new score. */
        void updateScore(int score) { scor.setText(""+ score); }

        /** Highlight the assiciated player as on turn. */
        void activate()   { name.setBold(true ); scor.setBold(true ); }
        /** Unhighlight the assiciated player as not on turn. */
        void deactivate() { name.setBold(false); scor.setBold(false); }
    }

    /** Simple display element to remeber, update and show the current number of tiles left to place. */
    private class CardsLeft extends HUDElement {
        GText text; int left;
        CardsLeft(float x, float y, int numberTiles) {
            super(x, y);

            text = new GText(0,0, "---");
            left = numberTiles+1;
            decr();
            addChild(text);

            text.setFill(Color.GRAY);
        }
        /** Update the internal representation to show a number on less than before. */
        void decr() { text.setText("Tiles Left:  " + --left); }
    }
}
