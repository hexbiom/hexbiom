package doerfer.hexbiom.gui.animation;

import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;

import static doerfer.hexbiom.gui.animation.Path.Change.*;

/** Schedules the animation of any number of parameters. DimType is used to address those parameters independently. */
public class Path<DimType extends Enum<DimType>> {

    /** Abstracts ways to combine old and new values to a restult. */
    public enum Change {
        /** REL is addition to the old value. MUL is multiplication of both. ABS is just replacing the old be the new value. */
        REL, MUL, ABS;
        /** Combine the values old and val using the the Change type specified by ct.  */
        static double change(Change ct, double old, double val) {
            switch (ct) {
                case REL: return old + val;
                case MUL: return old * val;
                case ABS: return       val;
            } return 1; // unreachable (why cant java see this?)
        }
    }

    /** The Method that gets a DimType and returns the value of the respective parameter. */
    private final ToDoubleFunction<DimType> get;
    /** The Method that gets a DimType and a new value to set a respective parameter. */
    private final DoubleBiConsumer<DimType> set;

    /** Excplicity specify the subset of the parameters to control. Because due to type erasure i cant do DimType.values() (while DimType is generic). SHALL NOT BE EMPTY. */
    private final DimType[] types;

    /** The current parameter changes (see Step). */
    private Step current;
    /** The steps to follow (only seperated if pause is called). */
    private ConcurrentLinkedDeque<Step> steps = new ConcurrentLinkedDeque<>();

    /** The {@link Animator} to which to (un)register the Path. */
    private Animator animator;

    /** Just sets the respective fields, see their docs for info. */
    public Path(Animator animator,
                ToDoubleFunction<DimType> getter,
                DoubleBiConsumer<DimType> setter,
                DimType[] types
                ) {
        this.animator = animator;
        get = getter; set = setter;
        this.types = types;
    }

    /** Initialize the Path (call before everything else!) and register it to the animators schedule. */
    public Path<DimType> start() {
        current = new Step(0);
        animator.schedule(this::animate);
        return this;
    }

    /** Deletes all registered Steps and stops the animation. After this, call start before anything else! (Other methods will throw {@link NullPointerException} else.) */
    public void stop() {
        animator.unschedule(this::animate);
        current = new Step(0);
        steps.clear();
    }

    /** Like to, but extendTime defaults to false. */
    public Path<DimType> to(DimType D, double v, double dt, Change changeType) { return to(D, v, dt, changeType, false); }
    /** Like to, but extendTime defaults to true. */
    public Path<DimType> to_extend(DimType D, double v, double dt, Change changeType) { return to(D, v, dt, changeType, true); }


    /** Continue the motion to another target: Let the parameter denoted by D reach the value v (modified by changeType and current terminal value) in time dt from now on (if extendTime = false) or after the current end of the respective motion (extendTime = true). @return this object for method chaining. */
    public Path<DimType> to(DimType D, double v, double dt, Change changeType, boolean extendTime) {
        lastStep().extend(D, v, dt, changeType, extendTime);
        return this;
    }

    /** Schedule a pause of duration waitT (if abs = false) or until time waitT (if abs = true) at the end of the current motion (irreversible). @return this object for method chaining. */
    public Path<DimType> pause(double waitT, boolean abs) {
        var last = lastStep();
        last.waitT = abs ? waitT
            : waitT + last.endT;

        return this;
    }

    /** Get the step to currently be perfomed last with unset wait time. Will construct a new one with this doesnt exist. */
    private Step lastStep() {
        var last = steps.isEmpty() ? current : steps.peekLast();
        if (last.waitT < 0) return last;

        last = new Step(last);
        steps.addLast(last);
        return last;
    }

    /** Aggregate {@link SmoothStep}s for every parameter (DimType in type) for one continuous combined motion, and a motionless time thereafter. */
    private class Step {
        /** Provides a way to circumvent the necessity of finit named members by acessing throug a (generic) enum type. The {@link SmoothStep} coresponding to every parameter. */
        private EnumMap<DimType, SmoothStep> dims;
        /** The time to wait after all {@link SmoothStep}s have finished (after endT) and the time when all {@link SmoothStep}s are finished. */
        double waitT = -1, endT;
        /** Is true iff all {@link SmoothStep}s are completed. */
        boolean wait = true;

        // only called when pause was called
        /** Construct a Step by resuming another one. */
        Step(Step last) {
            endT = last.endT + Math.max(last.waitT, 0);
            dims = new EnumMap<>(types[0].getClass());

            for (DimType D : types) {
                var d = last.dim(D);
                double t0 = d.x1, v0 = d.y1, d0 = d.d1;

                dims.put(D, new SmoothStep(t0, t0, v0, v0, d0, 0));
            }
        }
        /** Construct a Step to start at time t0 from the start of the Animator. */
        Step(double t0) {
            endT = t0;
            dims = new EnumMap<>(types[0].getClass());

            for (DimType D : types) {
                var v0 = get.applyAsDouble(D);
                dims.put(D, new SmoothStep(t0, t0, v0, v0, 0, 0));
            }
        }
        /** Continue the motion to another target: Let the parameter denoted by D reach the value v (modified by changeType and current terminal value) in time dt from now on (if extendTime = false) or after the current end of the respective motion (extendTime = true) */
        void extend(DimType D, double v, double dt, Change changeType, boolean extendTime) {
            double t0 = animator.time();
            double t1 = extendTime ? Math.max(dim(D).x1, t0)+dt : t0+dt;

            double v1 = change(changeType, dim(D).y1, v);
            dim(D).extend(t0, t1, v1, 0);

            endT = Math.max(endT, t1);
            wait = false;
        }
        /** Set all with DimType associated parameters using the set field and the value returned by the coresponding {@link SmoothStep} for the specified time t. */
        void update(double t) {
            for (DimType D : types) {
                double v = dims.get(D).at(t);
                if (Double.isFinite(v)) set.go(D, v);
            }
        }
        /** Fix all with DimType associated parameters using the set field to the end value of the motion (eliminates offsets due to numeric errors). */
        void complete(double t) {
            update(t);
            wait = true;
        }
        /** Shorthand for acces to the parameters {@link SmoothStep} enum map. */
        SmoothStep dim(DimType D) { return dims.get(D); }
    }

    /** Is performed ever frame by animator. See {@link Animator.Animation}. Updates the motions or sleeps as specified by the caller of "to" and "pause". */
    public void animate(double t, double dt) {
        if (current.wait) {
            if (t < current.waitT)      return;
            if (!steps.isEmpty())
                 current = steps.removeFirst();
            else current = new Step(t);
        } else if (t > current.endT)    current.complete(t);
        else                            current.update(t);
    }
}
