package doerfer.hexbiom.gui.animation;

import java.util.concurrent.*;

/** Perdiodically execute some Functions providig timing information.  */
public class Animator extends Thread {

    /** Animation.play may run any exception free code, given the total elapsed time t since the start of the Animator and the elapsed time since the Animation was last run (or since the start) dt. */
    @FunctionalInterface public interface Animation { void play(double t, double dt); }

    /** Milliseconds to spend at least per frame. Absolute start time and thereto relative endtime for the Animators activity in milliseconds. */
    private double mspf, start, endt = Double.POSITIVE_INFINITY;

    /** Every {@link Animation} in schedule is executed once every animation loop (= frame). SchedAdd and schedDel provide deferred schedule manipulation to avoid deadlocks :) */
    private ConcurrentLinkedDeque<Animation> schedule = new ConcurrentLinkedDeque<>(),
        schedAdd = new ConcurrentLinkedDeque<>(), schedDel = new ConcurrentLinkedDeque<>();

    /** Dont forget to call start to start the animation loop. */
    public Animator(double fps) { mspf = 1000 / fps; }
    /** get the time in milliseconds elapsed since the start of the animator */
    public double time() { return System.currentTimeMillis() - start; }

    /** Calls the animating functions in schedule with as precise as possible timing. Updates the schedule every frame. Sleeps between every loop over the schedule to keep the framerate above the one specified by mspf. */
    @Override
    public final void run() {
        try {
            start = time();     endt += start;
            double now, last = start;
            while (true) {
                now = time();
                if (now > endt) break;

                for (var a : schedule)
                    a.play(now, now - last);

                last = now;

                // update schedule removes
                schedule.removeAll(schedDel);
                schedDel.clear();

                // update schedule additions
                schedule.addAll   (schedAdd);
                schedAdd.clear();

                // Sleeps between every loop over the schedule to keep the framerate above the one specified by mspf.
                sleep((long) Math.max(0, mspf - (time() - last)));
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    /** Set the milliseconds since start after wich the process shall terminate. */
    public void setEnd(double endt) { this.endt = endt; }

    /** Add a {@link Animation} to the schedule at the end of the current frame. */
    public void   schedule(Animation a) { schedAdd.add(a); }
    /** Remove a {@link Animation} from the schedule at the end of the current frame. */
    public void unschedule(Animation a) { schedDel.add(a); }
}
