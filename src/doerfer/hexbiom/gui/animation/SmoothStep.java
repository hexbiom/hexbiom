package doerfer.hexbiom.gui.animation;

import java.util.*;
import java.util.concurrent.*;

/** Perfom smooth interpolations between values at fixed coordinates and derivatives using the common SmoothStep function (see Wikipedia). */
public class SmoothStep {

    /** a,b,c,d actually specify the cubic polynomial used for interpolation. The others specif the endpoint coordinates and derivatives. */
    double a,b,c,d, x0,x1,y0,y1, d0,d1;

    

    /** Overwrite the interpolation polynomial to match the old one at position x in value and derivative, but going to the new endpoint (x1|y1) with derivative d1. */
    public void extend(double x, double x1, double y1, double d1) {
        set(x, x1, at(x), y1, atD(x), d1);
    }

    /** Initialize a constant. */
    public SmoothStep() {
        set(0, 0, 0, 0, 0, 0);
    }

    /** Initialize a cubic polynomial coming from the startpoint (x0|y0) with derivative d0 and going to the endpoint (x1|y1) with derivative d1. */
    public SmoothStep(double x0, double x1, double y0, double y1, double d0, double d1) {
        set(x0, x1, y0, y1, d0, d1);
    }

    /** Compute a cubic polynomials parameters (modifies a,b,c,d) coming from the startpoint (x0|y0) with derivative d0 and going to the endpoint (x1|y1) with derivative d1. */
    public void set(double x0, double x1, double y0, double y1, double d0, double d1) {
        this.x0 = x0; this.x1 = x1; this.y0 = y0; this.y1 = y1; this.d0 = d0; this.d1 = d1; 

        double p0 = Math.pow(x0 - x1, -3);
        double p1 = d0*x0;
        double p2 = d1*x0;
        double p3 = d0*x1;
        double p4 = d1*x1;
        double p5 = 3*y0;
        double p6 = p5*x0;
        double p7 = 3*y1;
        double p8 = p7*x1;
        double p9 = Math.pow(x0, 2);
        double p10 = d0*p9;
        double p11 = Math.pow(x1, 2);
        double p12 = 2*p11;
        double p13 = 2*p9;
        double p14 = d1*p11;
        double p15 = Math.pow(x0, 3);
        double p16 = 6*x0*x1;
        double p17 = Math.pow(x1, 3);

        a = p0*(p1 + p2 - p3 - p4 - 2*y0 + 2*y1);
        b = -p0*(-d0*p12 + d1*p13 + p1*x1 + p10 - p14 - p2*x1 - p5*x1 - p6 + p7*x0 + p8);
        c = p0*(-d0*p17 + d1*p15 - p1*p11 - p12*p2 + p13*p3 - p16*y0 + p16*y1 + p4*p9);
        d = -p0*(-p1*p17 + p10*p11 - p11*p6 - p14*p9 + p15*p4 - p15*y1 + p17*y0 + p8*p9);
    }

    /** Evaluate the function at x. */
    public double at (double x) { 
        if (x < x0) return y0;
        

        return x > x1 ? y1 : d + x * (c + x * (b + x * a)); 
    }

    /** Evaluate the derivative of the function at x. */
    public double atD(double x) { 
        if (x < x0) return d0;
        

        return x > x1 ? d1 : c + x * (2 * b + x * 3*a); 
    }
}
