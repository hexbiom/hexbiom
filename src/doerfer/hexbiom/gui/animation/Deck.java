package doerfer.hexbiom.gui.animation;

import java.util.*;

/** A fixed size circular container - every data structure that somehow mirrors players or turns shall use this, to provide easy (idiotproof) and *efficient* access to such a fixed size rotational queue. */
public class Deck<E> extends ArrayList<E> {

    /** Since an Array is used under the hood, we remember the current offset position - the reference index. */
    private int at = 0;

    /** Construct the fixed size container and initialize everything in it with null(!). */
    public Deck(int size) {
        super(size);
        for (int i = 0; i < size; i++)
            add(null);
    }

    /** Since an Array is used under the hood, we remember the current offset position - the reference index. */
    public int getOffset() { return at; }

    /** Compute the index in the array using the offset "at" and wrapping over the arrays size.  */
    private int ind(int index) { return Math.floorMod(index + at, Math.max(size(),1)); }

    /** Move the focus to the current element one further. */
    public void shift(int n) {
        at = ind(n);
    }

    /** Get the element currently in focus. */
    public E top()    { return get(0); }
    /** Get the element currently at last in focus. */
    public E bottom() { return get(-1); }

    /** Get the element relative to the current focus position. Allows negative indicee like in python. */
    @Override
    public E get(int index) {
        if (size() == 0) throw new IndexOutOfBoundsException("Deck is empty.");
        return super.get(ind(index));
    }

    /** Get the element relative to the current focus position. Allows negative indicee like in python. @return the old element there (may be null!). */
    @Override
    public E set(int index, E element) {
        return super.set(ind(index), element);
    }

    /** Returns the element at the current focus position and writes null at its position */
    public E pop() {
        return set(0, null);
    }

    /** Places the element at the current focus position and teps the focus one further. @return the element previously there. */
    public E next(E element) {
        shift(1);
        return set(-1, element);
    }
}
