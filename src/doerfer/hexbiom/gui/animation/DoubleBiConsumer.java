package doerfer.hexbiom.gui.animation;

/** A {@link FunctionalInterface} accepting a T and a double, but returning nothing. This exists to prevent autoboxing that would happen with BiConsumer<T,Double>, thus providing a perfomance increase. */
@FunctionalInterface
public interface DoubleBiConsumer<T> {
     void go(T x, double f);
}
