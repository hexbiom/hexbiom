package doerfer.hexbiom.gui.animation;

import doerfer.preset.graphics.*;

import static doerfer.hexbiom.gui.animation.Path.Change;
import static doerfer.hexbiom.gui.animation.Path.Change.*;
import static doerfer.hexbiom.gui.animation.Sprite.Dim.*;   // funny that this is correct

/** Wrap a {@link GElement} of type E to be animated with a {@link Path} parametrized by the Dimensions Dim. */
public class Sprite<E extends GElement> {

    /** X and Y = coordinates, S = scale, R = rotation, O = opacity */
    public enum Dim { X,Y,S,R, O }

    /** Save the values of the dimensions. */
    protected double x=0, y=0, s=1, r=0, o=1;

    /** The {@link GElement} of type E whichs attributes are wrapped by this Sprite. */
    protected final E element;
    /** The {@link Path} in charge of animating the parameters. */
    private final Path<Dim> path;
    /** The {@link GGroup} that currently holds the {@link GElement} element (null if none). */
    protected GGroup group;
    /** Save the extension of the GElement to provide centered positioning. */
    protected float WIDTH, HEIGHT;

    /** Construct a Sprite to animate the {@link GElement} element using the {@link Animator} animator. width and height set WIDTH and HEIGHT to provide centered positioning. */
    public Sprite(Animator animator, float width, float height, E element) {
        path = new Path<>(animator, this::get, this::set, Dim.values());
        this.element = element;
        WIDTH = width;
        HEIGHT = height;
    }

    /** Get the wrapped {@link GElement}. */
    public E elem() { return element; }

    /** Change the {@link GGroup} the wrapped {@link GElement} element is associated to (can only be one, which is ensured herewith.) Makes the thing invisible of group = null. */
    public void visible(GGroup group) {
        if (this.group != null)
            this.group.removeChild(element);
        if (group != null)
            group.addChild(element);
        this.group = group;
    }

    /** Call this before any animation modification. Start the Animation at the specified parameter values and be visible in group. */
    public void start(GGroup group, double x, double y, double s, double r, double o) {
        set(X, x); set(Y, y); set(R, r); set(S, s); set(O, o);
        path.start();
        visible(group);
    }

    /**  Call this to enable garbage collection for the element. Call start thereafter if you want to modify the animations again! */
    public void stop() {
        visible(null);
        path.stop();
    }

    /** Schedule a motionless phase of length dt in milliseconds after the last animation. */
    public Sprite<E> pause(double dt) {
        path.pause(dt, false);
        return this;
    }

    /** See the corresponding {@link Path} method. Have start called before! */
    public Sprite<E> to_extend(Dim D, double v, double dt, Change changeType) {
        path.to(D, v, dt, changeType, true); return this; }
    /** See the corresponding {@link Path} method. Have start called before! */
    public Sprite<E> to(Dim D, double v, double dt, Change changeType) {
        path.to(D, v, dt, changeType, false); return this; }

    /** See the corresponding {@link Path} method. Just combines all parameters to be set in time t from now on to those values. Have start called before! */
    public Sprite<E> to(double x, double y, double s, double r, double o, double t) {
        path.to(X, x, t, ABS).to(Y, y, t, ABS).to(S, s, t, ABS).to(R, r, t, ABS).to(O, o, t, ABS);
        return this;
    }

    /** Provides the mapping between Dim type and the coresponding parameter. */
    public double get(Dim type)  {
        switch (type) {
            case X: return x;
            case Y: return y;
            case S: return s;
            case R: return r;
            case O: return o;
        }
        throw new UnsupportedOperationException("todo implement get for "+type);    // UNCALLABLE
    }

    // Package private so that only path may set this (else it will go out of sync) would be best, but i want gtile another package to overwrite it in.
    /** Provides the mapping between Dim type and the coresponding parameter. Applies transformations to the {@link GElement}. Override for costum functionality (like special centered rotation for {@link doerfer.hexbiom.gui.GTile}s) */
    public void set(Dim type, double v)  {
        switch (type) {
            case X: x = v; trans(); break;
            case Y: y = v; trans(); break;
            case S: s = v;
                assert s!=0;
                element.transform().scale((float) s, (float) s);
                trans(); // update the translation, because with size change the center position changes
                break;
            case R: r = v;
                element.transform().rotate((float) r);
                // TODO center rotation
                break;
            case O: o = v;
                element.setOpacity((float) o);
        }
    }

    /** Translate element to have its center at (x,y). */
    protected void trans() {
        element.transform().translate(
            (float) (x - s * WIDTH  / 2),
            (float) (y - s * HEIGHT / 2));
    }

}