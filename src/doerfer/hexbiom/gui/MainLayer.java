package doerfer.hexbiom.gui;

import java.awt.event.*;
import java.util.HashSet;
import java.util.Map.*;

import java.util.Set;


import doerfer.preset.*;
import doerfer.preset.graphics.*;

import doerfer.hexbiom.board.*;
import doerfer.hexbiom.config.*;
import doerfer.hexbiom.gui.animation.*;

import static doerfer.hexbiom.gui.animation.Path.Change.*;
import static doerfer.hexbiom.gui.animation.Sprite.Dim.*;

/**
 * this class stores display and interactive functionality for human turns and viewing current board state
 */
public class MainLayer extends Layer {

    // (logic) the currently legal moves for the active player
    Set<TilePlacement> validTilePlacements;

    // (graphic) all valid moves for whatever rotation
    Set<DrawableTile> currentValidMoves = new HashSet<>();

    // (interactive) activated moves for a given rotation

    Set<DrawableTile> currentActivated = new HashSet<>();
    private final ViewPort viewPort;


    public MainLayer(Interface root, Animator renderer, GPanel panel, Board board, Config conf) throws GUIException {
        super(root, renderer, panel, board, panel.getLayerMain(), conf);
        viewPort = new ViewPort(group);

        for (Entry<Coords, BiomeTile> e : board.getGrid().entrySet()) {
            if(e.getValue() != null){
                placeTile(new TilePlacement(e.getKey().getOddqRow(), e.getKey().getOddqColumn(), 0), e.getValue());
            }
        }
    }

    /**
     * display dark grey hexagon for every move which is theoretically possible for this human player and tile
     */
    public void showAllValidMoves() throws GUIException {
        // display new legal moves (grey)
        Set<Coords> uniqueXY = new HashSet<>();
        for (TilePlacement move: validTilePlacements) {
            // for every x,y only one grey element is added
            if(uniqueXY.add(new Coords(move))) { // false if x,y already exists
                DrawableTile dt;


                dt = GTile.biomeTile2drawable(null, panel);


                dt.setOpacity(0.3F); //  set to dark grey
                currentValidMoves.add(dt); // add to list to easy remove later
                placeDrawableTile(move, dt); // adds to group and positions on map
            }
        }
        activateMoves(root.listenedRotation); // activate for rotation 0 at the beginning of the turn
    }

    /**
     * hides all dark grey validMoves
     * <p>is called whenever human turn/input ends</p>
     */
    public void hideValidMoves() {
        // remove former valid moves
        for (DrawableTile oldValid: currentValidMoves) {
            group.removeChild(oldValid);
        }
        currentValidMoves.clear();
    }

    /**
     * creates interactive clickable white hexagons for a given rotation, so that humans can input a move
     * @param rotation the roation listened in {@link Interface}
     */
    public void activateMoves (int rotation) throws GUIException {

        deactivateMoves();

        for (TilePlacement move: validTilePlacements) {
            if(move.getRotation() == rotation) {
                DrawableTile dt;

                dt = GTile.biomeTile2drawable(null, panel);

                currentActivated.add(dt);


                dt.setMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        if (root.isListening()) {
                            root.listenedTP = new TilePlacement(move.getRow(), move.getColumn(), root.listenedRotation);
                        }
                    }
                });

                placeDrawableTile(move, dt);

            }
        }

    }

    /**
     * this method is called whenever a move by human is done and interactive elements needs to be removed
     */
    public void deactivateMoves() {
        for (DrawableTile oldActivated: currentActivated) {
            group.removeChild(oldActivated);
        }
        currentActivated.clear();
    }

    /**
     * Like the other placeTile but lets the {@link GTile} tile fly to its new position.
     */
    public void placeTile(TilePlacement tp, GTile tile) {

        var pid = root.getActivePlayerID();
        if (pid != 0) tile.elem().setStroke(conf.playerColors.get(pid-1));

        var w = DrawableTile.WIDTH;
        var h = DrawableTile.HEIGHT;
        var centerH = GPanel.VIEWPORT_HEIGHT/2;
        var centerW = GPanel.VIEWPORT_WIDTH/2;

        double s = viewPort.get(S);
        double x0 = (tile.get(X) - viewPort.get(X))/s + centerW,
               y0 = (tile.get(Y) - viewPort.get(Y))/s + centerH,
               s0 = tile.get(S) / s,
               r0 = tile.get(R) - viewPort.get(R),
               o0 = tile.get(O);

        double x1 = w/2 + centerW+tp.getColumn() * (w * 0.75),
               y1 = h/2 + ((tp.getColumn()&1) == 1
                  ? centerH+(tp.getRow() * h+h/2)
                  : centerH+tp.getRow() * h),
               s1 = 1,
               r1 = 360 + 60*tp.getRotation(),
               o1 = 1;

        tile.start(group,x0, y0, s0, r0, o0);
        tile.to(x1, y1, s1, r1, o1, 2000);
    }

    /**
     * this method wraps up code to translate a tile to a drawable tile (with given rotation from tp), adds the players color and calls {@link MainLayer#placeDrawableTile(TilePlacement, DrawableTile)}
     * @param tp the coords where to place, its rotation is used here
     * @param tile the biomes and the playerID are used here to make a DrawableTile from it
     */
    public void placeTile(TilePlacement tp, BiomeTile tile) throws GUIException {
        DrawableTile dt = GTile.biomeTile2drawable(tile.turn(tp.getRotation()), panel);
        placeDrawableTile(tp, dt);
        if(tile.getPlayerID() != 0) dt.setStroke(conf.playerColors.get(tile.getPlayerID()-1));
    }

    /**
     * tiles are placed in the center of the gpanel, according to oddq-layout for hexagonal structures
     * @param tp where the tile should be placed
     * @param dt a drawableTile (previously created) to be placed
     */
    public void placeDrawableTile(TilePlacement tp, DrawableTile dt) {

        group.addChild(dt);
        var w = DrawableTile.WIDTH;
        var h = DrawableTile.HEIGHT;
        var centerH = GPanel.VIEWPORT_HEIGHT/2;
        var centerW = GPanel.VIEWPORT_WIDTH/2;

        if((tp.getColumn()&1) == 1){ // odd column
            dt.transform().translate((float) (centerW+tp.getColumn() * (w * 0.75)), centerH+((tp.getRow() * h+h/2)));
        } else {
            dt.transform().translate((float) (centerW+tp.getColumn() * (w * 0.75)), centerH+tp.getRow() * h);
        }
    }

    /**
     * whenever MouseWheel action is noticed this method get called and updates the clickable moves for the new rotation
     * @param rotation the rotation listened in {@link Interface}
     */
    public void notifyMouseWheel(int rotation) throws GUIException {
        activateMoves(rotation);
    }


    /** Enlarge the main view (keeping it centered) (animated). */
    public void zoomIn() { viewPort.to(S,   1.5, 500, MUL); }
    /** Downsize the main view (keeping it centered) (animated). */
    public void zoomOut(){ viewPort.to(S, 1/1.5, 500, MUL); }

    /** Return the main view to its original position (animated). */
    public void resetView(){ viewPort.center(1000); }

    /** Move the view in the specified direction (animated). */
    public void moveView(KeyListener.arrowKey direction){
        switch (direction){
            case UP:    viewPort.to(Y, +200, 500, REL); break;
            case DOWN:  viewPort.to(Y, -200, 500, REL); break;
            case LEFT:  viewPort.to(X, +200, 500, REL); break;
            case RIGHT: viewPort.to(X, -200, 500, REL); break;
        }
    }

    /** Use the {@link Sprite} animation facility to control the main view.  */
    private class ViewPort extends Sprite<GGroup> {

        /** The Object this sprite wraps is the {@link GGroup} of the {@link MainLayer}. The group field of {@link Sprite} is thus unset. */
        ViewPort(GGroup group) {
            super(renderer, GPanel.VIEWPORT_WIDTH, GPanel.VIEWPORT_HEIGHT, group);
            start(null, GPanel.VIEWPORT_WIDTH/2F, GPanel.VIEWPORT_HEIGHT/2F, 1, 0, 1);
        }
        /** Transform the {@link GGroup} of the {@link MainLayer} to show its center. */
        void center(double dt) { to(GPanel.VIEWPORT_WIDTH/2F, GPanel.VIEWPORT_HEIGHT/2F, 1, 0, 1, dt); }
    }
}
