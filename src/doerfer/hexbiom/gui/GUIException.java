package doerfer.hexbiom.gui;


import java.io.IOException;
import com.kitfox.svg.SVGElementException;

/** Combine the Exceptions the presets svg library me throw at us - effectively dooming the visual frontend. */
public class GUIException extends Exception {
    public GUIException(SVGElementException e) {
        super("Corrupted graphics assets! Please ensure you run an intact copy of the software, that is not disrupted at runtime!", e);
    }
    public GUIException(IOException e) {
        super("Corrupted graphics assets! Please ensure you run an intact copy of the software, that is not disrupted at runtime!", e);
    }
}
