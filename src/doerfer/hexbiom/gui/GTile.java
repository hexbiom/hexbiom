package doerfer.hexbiom.gui;

import java.io.IOException;

import com.kitfox.svg.*;

import doerfer.hexbiom.*;
import doerfer.hexbiom.gui.animation.*;
import doerfer.hexbiom.board.BiomeTile;

import doerfer.preset.*;
import doerfer.preset.graphics.*;


import static doerfer.preset.graphics.PresetTileComponent.*;
import static doerfer.hexbiom.gui.animation.Sprite.Dim.*;
import static doerfer.hexbiom.gui.animation.Sprite.Dim;


/** Simple wrapper for a {@link DrawableTile} to be animated as a {@link Sprite}. */
public class GTile extends Sprite<DrawableTile> {

    public GTile(Animator animator, BiomeTile btile, GPanel panel) throws GUIException {
        this(animator, biomeTile2drawable(btile, panel));
    }
    public GTile(Animator animator, DrawableTile tile) {
        super(animator, DrawableTile.WIDTH, DrawableTile.HEIGHT, tile);
    }

    /** See set of {@link Sprite}. This overwrites rotation to be the centerrotation jake provided for {@link DrawableTile}. */
    @Override
    public void set(Dim type, double v) {
        if (type == R) {
            r = v;
            element.rotate((float) r);
        }
        else super.set(type, v);
    }

    /** Makes a drawable svg thingy out of a {@link BiomeTile}. Needs to be added to a group, if you want to see it. */
    public static DrawableTile biomeTile2drawable(BiomeTile btile, GPanel panel) throws GUIException  {
        try {
            var tile = new DrawableTile(panel);

            if (btile == null)
                tile.addComponent(new DrawableTileComponent(PresetTileComponent.GREY, 0f));

            else {
                float r = 0;
                for (var e : btile.getEdges())
                    tile.addComponent(new DrawableTileComponent(edge2comp(e), (r++)*60f));

                tile.addComponent(new DrawableTileComponent(center2comp(btile.getCenter()), 0));
            }

            return tile;
        } catch (IOException e)         { throw new GUIException(e); }
          catch (SVGElementException e) { throw new GUIException(e); }
        // NOTE: multiple catch (mit |) casted e automatisch zu Exception, woduruch der konstruktor nichtt mehr funktioniert, obwohl absolut klar ist dass es nur eine der beide gecatchten excpetions sein kann??
    }

    /** Maps Biome to {@link PresetTileComponent}s of edges. */
    public static PresetTileComponent edge2comp(Biome edge) {
        switch (edge) {
            case WATER:       return WATER_EDGE;
            case FIELDS:      return FIELDS_EDGE;
            case FOREST:      return FOREST_EDGE;
            case HOUSES:      return HOUSES_EDGE;
            case PLAINS:      return PLAINS_EDGE;
            case TRAINTRACKS: return TRAINTRACKS_EDGE;
        }   return null;      // NEVER CALLED
    }

    /** Maps Biome to {@link PresetTileComponent}s of centers. */
    public static PresetTileComponent center2comp(Biome center) {
        switch (center) {
            case WATER:       return WATER_CENTER;
            case FIELDS:      return FIELDS_CENTER;
            case FOREST:      return FOREST_CENTER;
            case HOUSES:      return HOUSES_CENTER;
            case PLAINS:      return PLAINS_CENTER;
            case TRAINTRACKS: return TRAINTRACKS_CENTER;
        }   return null;      // NEVER CALLED
    }
}