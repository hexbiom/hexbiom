package doerfer.hexbiom.gui;

import java.awt.event.KeyEvent;

/**
 * this class is used to listen all key interactions by users, and is added to frame
 * <p>a key press results in calling a method in mainLayer (for camera actions) or in gui (for exit after game over)</p>
 */
public class KeyListener implements java.awt.event.KeyListener {

    MainLayer map;
    Interface gui;


    /**
     * Use constructor with reference to the mainLayer and the gui
     * @param map the {@link MainLayer} instance used
     * @param gui the {@link Interface} instance used
     */
    public KeyListener(MainLayer map, Interface gui){
        this.map=map;
        this.gui = gui;
    }

    @Override
    public void keyTyped(KeyEvent e) {
        // not used
    }

    /**
     * used to pass the direction of the pressed arrow to the mainLayer
     */
    public enum arrowKey {
        UP, DOWN, LEFT, RIGHT
    }

    /**
     * calls the method in MainLayer and Interface to perform camera action or exit game after game over or to rotate by key
     * @param e the key pressed
     * @see MainLayer#zoomIn()
     * @see MainLayer#zoomOut()
     * @see MainLayer#resetView()
     * @see MainLayer#moveView(arrowKey)
     * @see Interface#exitGame()
     * @see Interface#keyRotation(int)
     */
    @Override
    public void keyPressed(KeyEvent e) {
        switch(e.getKeyChar()){
            case '+':
                map.zoomIn();
                break;
            case '-':
                map.zoomOut();
                break;
            case '0':
                map.resetView();
                break;
            case 'x':
                gui.exitGame();
                break;
            case 'q':
                gui.keyRotation(-1);
                break;
            case 'e':
                gui.keyRotation(1);
                break;
        }

        switch(e.getKeyCode()){
            case KeyEvent.VK_UP:
                // arrow up
                map.moveView(arrowKey.UP);
                break;
            case KeyEvent.VK_DOWN:
                // arrow down
                map.moveView(arrowKey.DOWN);
                break;
            case KeyEvent.VK_LEFT:
                // arrow left
                map.moveView(arrowKey.LEFT);
                break;
            case KeyEvent.VK_RIGHT:
                // arrow right
                map.moveView(arrowKey.RIGHT);
                break;
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        // not used
    }

}
