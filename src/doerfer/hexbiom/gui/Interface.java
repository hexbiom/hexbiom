package doerfer.hexbiom.gui;


import java.util.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import com.kitfox.svg.*;

import doerfer.preset.*;
import doerfer.preset.graphics.*;


import doerfer.hexbiom.board.*;
import doerfer.hexbiom.config.*;
import doerfer.hexbiom.player.*;
import doerfer.hexbiom.gui.animation.*;
import doerfer.hexbiom.gui.animation.Path.Change;
import doerfer.hexbiom.gui.animation.Sprite.Dim;


/** EXPLANATION:
The same game events from Main call similar functions like with a Player.
Interface then updates logic stuff partly the same as player (like keeping track of current card stack (→ move functionality to abstract class Attender)).
And then calls some methods on hud and map designed to properly reflect stuff visually.
*/

public class Interface extends Attender implements GameView {

    private final Animator renderer;
    private final JFrame  frame;
    private final GPanel panel;
    private final HudLayer hud;
    private MainLayer map;
    private final Config conf;
    private boolean listening = false;    // wenn das true ist, sollen die mouseevents sachen auslösen, wenn false nicht
    int listenedRotation = 0;
    TilePlacement listenedTP;
    private boolean gameOver = false;

    public Interface(Config conf) throws GUIException {
        this.conf = conf;
        init_(conf);

        frame = new JFrame();

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(1280, 720);

        try {
            panel = new GPanel();
        } catch (SVGElementException e) {
            throw new GUIException(e);
        }
        frame.add(panel);

        java.awt.Color bgColor = new Color(0,20,20);
        panel.setBgColor(bgColor);

        frame.setLocationRelativeTo(null);  // start centered on monitor
        frame.setVisible(true);
        panel.updateScale();

        renderer = new Animator(60);
        renderer.schedule((t,dt) -> frame.repaint());
        renderer.start();

        hud = new HudLayer(this, renderer, panel, board, conf);
    }

    /** call after all cards are uncovered  */
    public void initDisplay() throws GUIException {

        map = new MainLayer(this, renderer, panel, board, conf);


        // MouseRad für Drehung der aktiven Karte
        frame.addMouseWheelListener( e -> {
            if (!isListening()) return;

            listenedRotation = Math.floorMod(listenedRotation + e.getWheelRotation(), 6);
            notifyMouseWheel();
        });

        // Tasten für Kamera, Zoomen, resetView
        frame.addKeyListener(new KeyListener(map, this));

        // Fenster kann kleiner, größer gezogen werden
        frame.getRootPane().addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                super.componentResized(e);
                panel.updateScale();
            }
        });

        hud.preGame = false;
    }


    /** called by Listeners to ask if they should do something */
    public boolean isListening() { return listening; }

    /**
     * this method is called whenever a new tile is uncovered, it is added to queue and displayed in HUD
     * @param tile the tile just uncovered
     * @throws GUIException if
     */
    public void notifyNewUncoveredTile(Tile tile) throws GUIException {
        notifyNewUncoveredTile_(tile);
        hud.uncoverTile(BiomeTile.from(tile, 0));
    }

    /**
     * calls {@link HudLayer} and {@link MainLayer} to notify about the {@link Interface#listenedRotation}
     */
    public void notifyMouseWheel() {
        hud.notifyMouseWheel(listenedRotation);
        try {
            map.notifyMouseWheel(listenedRotation);
        } catch (GUIException e) {
            displayError(e);
            gameOver(e);
        }

    }


    /** Display an error message. */
    @Override
    public void displayError(Exception e) {
        String msg =e.getMessage();
        hud.message(msg, LogLevel.ERRORS);
    }

    /**
     * this method is called when the game ends as intended or by cheat exception, if an exception was the reason, it is displayed
     * <p>after it is called the 'x' to exit key is activated and 60 seconds will pass before application shuts itself down</p>
     * @param e the exception that causes game end, null if game ends as intended
     */
    public void gameOver(Exception e) {
        gameOver = true; // used to activate key 'x' for exit
        hud.gameOver(e);
        try {
            Thread.sleep(180000);
        } catch (InterruptedException ex)  {
            Thread.currentThread().interrupt();
        }
        System.exit(e == null ? 0 : 2);
    }

    /**
     * this method is called whenever a human is taking his turn. It is called after {@link Interface#setValidTilePlacements(Set)}
     * clicking the displayed interactive white tiles results in constructing a TilePlacement instance which results on ending the wait for input loop
     * @return the performed input (clicked white tile(x,y) with listenedRotation)
     */
    @Override
    public TilePlacement requestTilePlacement() {
        listenedRotation = 0;

        try {
            map.showAllValidMoves();
        } catch (GUIException e) { // IO and SVG-Element exceptions result in game end
            displayError(e);
            gameOver(e);
        }


        while (true) {
            listenedTP = null;
            listening = true;

            while (listenedTP == null) {
                try { Thread.sleep(1);
                } catch (InterruptedException e) { Thread.currentThread().interrupt(); }   // best practice, please copy
            }
            map.hideValidMoves();
            map.deactivateMoves();
            listening = false;

            return listenedTP;
        }
    }

    /**
     * this method is called whenever a human is about to take a turn and the valid position needs to be displayed and activated
     * <p>the instance of human tells the gui which his options are!</p>
     * @param validTilePlacements the list of valid TilePlacements
     *
     */
    @Override public void setValidTilePlacements(Set<TilePlacement> validTilePlacements) {
        map.validTilePlacements = validTilePlacements;
    }

    /**
     * this method is called whenever a TilePlacement is performed. It uses {@link Attender#queue} to determine active player and current tile
     *
     * <p>this calls {@link MainLayer#placeTile(TilePlacement, BiomeTile)} and {@link HudLayer#updateScores()}</p>
     * @param tp the chosen location and rotation
     */
    public void notifyTilePlacement(TilePlacement tp) {
        GTile placedTile = hud.placeTile(tp, queue.peek());

        if (tp != null && placedTile != null)  { // no skip
            map.placeTile(tp, placedTile);
        }

        notifyTilePlacement_(tp);   // Keep this order

        hud.updateScores(); // needs to be called out of hud.placeTile to update after the board obj was altered

    }

    /**
     * this method is used to rotate with keyPresses,
     * alternative to rotation by mouseWheel
     * @param direction the listened direction of rotation
     */
    public void keyRotation(int direction) {
        if(!isListening()) return;
        listenedRotation = Math.floorMod(listenedRotation + direction, 6);
        notifyMouseWheel();
    }

    /**
     * is used to exitGame with keypress X after gameOver
     */
    public void exitGame() {
        if(gameOver){
            System.exit(0);
        }
    }



    // unused gameView methods, Jake said those are optional
    @Override
    public void focus(TilePlacement location) { throw new UnsupportedOperationException(); }
    @Override public void setGameOver(boolean gameOver) { throw new UnsupportedOperationException(); }
    @Override public void placeTile(Tile tile, TilePlacement tilePlacement, Player owner) { throw new UnsupportedOperationException(); }
    @Override public void setActivePlayer(Player player) { throw new UnsupportedOperationException(); }
    @Override public void setPlayers(java.util.List<Player> players, java.util.List<Color> playerColors) { throw new UnsupportedOperationException(); }
    @Override public void setPlayerSkipped(Player player, boolean skipped) { throw new UnsupportedOperationException(); }
    @Override public void setTilesLeft(int n) { throw new UnsupportedOperationException(); }
    @Override public void setUncoveredTiles(java.util.List<Tile> tiles) { throw new UnsupportedOperationException(); }

}

