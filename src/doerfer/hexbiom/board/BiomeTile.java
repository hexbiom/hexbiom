package doerfer.hexbiom.board;

import doerfer.preset.*;

import java.util.*;

public class BiomeTile extends doerfer.preset.Tile {

    private int playerID;


    /**
     * Constructs a new Tile based on a given 6-Tuple.
     *
     * @param tile#edges The 6-Tuple containing the 6 edges.
     *              <p>
     *              <p>
     *              The following illustration shows how the list elements correspond to the edges of an actual hexagonal Tile.
     *
     *              <pre>
     *                                                          __0__
     *                                                         /     \
     *                                                      5 /       \ 1
     *                                                       /         \
     *                                                       \         /
     *                                                      4 \       / 2
     *                                                         \_____/
     *                                                            3
     *                                                     </pre>
     * @see TileGenerator
     */

    public static BiomeTile from(Tile tile, int pID) {
        List<Biome> edges = new ArrayList<>(6);
        for (int e = 0; e < 6; e++) edges.add(tile.getEdge(e));
        return new BiomeTile(edges, pID);
    }

    public BiomeTile(List<Biome> edges, int playerID) {
        super(edges);
        this.playerID = playerID;
    }

    public BiomeTile(List<Biome> edges) {
        super(edges);
        this.playerID = 0;
    }

    /**
     * method to compare two {@link BiomeTile} s
     * checks whether two {@link BiomeTile}s have matching {@link Biome}s
     * @param tile to compare to
     * @return true if biomes are the same
     */
    public boolean compareBiomes(BiomeTile tile) {
        for (int i = 0; i < 6; i++) {
            if (getEdge(i) != tile.getEdge(i)){
                return false;
            }
        }
        return true;



    }
    @Override
    public String toString() {
        return "BiomeTile{" +
                "playerID=" + playerID +
                ", edges=" + edges +
                '}';
    }

    public List<Biome> getEdges(){
        return this.edges;
    }

    public int getPlayerID() {
        return playerID;
    }

    public void setPlayerID(int playerID) {
        this.playerID = playerID;
    }

    /**
     *
     * @param rotation 0-5 how much tile should be turned
     * @return new BiomeTile with applied rotation
     */
    public BiomeTile turn(int rotation){
        List<Biome> rotatedEdges = new ArrayList<>(6);
        for (int e = 0; e < 6; e++) {
            rotatedEdges.add(this.getEdge(e));
        }
        Collections.rotate(rotatedEdges, rotation);
        return new BiomeTile(rotatedEdges, this.getPlayerID());
    }

    /**
     * determines the center of this Tile as stated in game rules
     * @return the determined center Biome
     */
    @Override
    public Biome getCenter() {
        if (edges.contains(Biome.TRAINTRACKS))
            return Biome.TRAINTRACKS;
        else {
            Map<Biome, Integer> edgeCounter = new HashMap<>();
            // insert all edges to hashmap and store frequency
            for (Biome edge : edges) {
                if (edgeCounter.containsKey(edge)) {
                    int freq = edgeCounter.get(edge);
                    freq++;
                    edgeCounter.put(edge, freq);
                } else {
                    edgeCounter.put(edge, 1);
                }
            }

            // find max frequency
            int max = 0;
            for (Map.Entry<Biome, Integer> val : edgeCounter.entrySet()) {
                if (max < val.getValue()) {
                    max = val.getValue();
                }
            }

            // List for Biomes, which occur the most
            List<Biome> most = new ArrayList<>();

            for (Biome countedEdge :
                    edges) {
                if (edgeCounter.get(countedEdge) == max) {
                    most.add(countedEdge);
                }
            }

            // decides center by game rules from most occurring biomes
            if (most.size() == 1) {
                return most.get(0);
            } else if (most.contains(Biome.WATER)) {
                return Biome.WATER;
            } else if (most.contains(Biome.PLAINS)) {
                return Biome.PLAINS;
            } else if (most.contains(Biome.FOREST)) {
                return Biome.FOREST;
            } else if (most.contains(Biome.HOUSES)) {
                return Biome.HOUSES;
            } else {
                return Biome.FIELDS;
            }
        }
    }
}
