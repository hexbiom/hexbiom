package doerfer.hexbiom.board;

import doerfer.preset.*;

import java.util.*;
import java.util.Map.Entry;

/**
 * <P>Board manages state of game and moves made </P>
 * constructing uses a {@link GameConfiguration}
 * current gamestate is stored in {@link Board#grid} in Cube Coords described here : <a href="https://www.redblobgames.com/grids/hexagons/#coordinates-cube">...</a>
 * new tiles are placed using first {@link Board#verifyMove(TilePlacement, BiomeTile)} then {@link Board#placeUnverified(TilePlacement, BiomeTile)}
 */

public class Board {

    private final Map<Coords, BiomeTile> grid = new HashMap<>();

    // next Edge in direction (direction == index (0-5)) to avoid typos with coordinates
    private final int[][] nEID = {{0,-1, +1}, {+1, -1, 0}, {+1, 0, -1}, {0, +1, -1}, {-1, +1, 0}, {-1, 0, +1}};
    /**
     * <P>Constructor takes config to initialize board with preplaced tiles</P>
     * uses: {@link GameConfiguration#getPreplacedTiles()}, {@link GameConfiguration#getPreplacedTilesPlayerIDs()}, {@link GameConfiguration#getPreplacedTilesPlacements()}
     *
     * @param configuration parsed {@link GameConfiguration}
     */
    public Board(GameConfiguration configuration) {
        List<TilePlacement> confTPs = configuration.getPreplacedTilesPlacements();
        List<Integer> confIDs = configuration.getPreplacedTilesPlayerIDs();
        List<Tile> confTiles = configuration.getPreplacedTiles();

        // translate tiles and their owners into BiomeTiles (those contain their owner)
        List<BiomeTile> preplacedBiomeTiles = new ArrayList<>(confTiles.size());
        for (int i = 0; i < confTiles.size(); i++) {
            preplacedBiomeTiles.add((BiomeTile) confTiles.get(i));
            preplacedBiomeTiles.get(i).setPlayerID(confIDs.get(i));
        }
        for (int i = 0; i < confTPs.size(); i++) {
            this.placeUnverified(confTPs.get(i), preplacedBiomeTiles.get(i));
        }
    }

    /**
     * <p> ONLY USE THIS FOR DISPLAY-BOARD, NOT FOR PLAYERS </p>
     * <p> this place-method contains no logic at all</p>
     * @param tp TilePlacement from which Coords is generated
     * @param tile Tile which is rotated, contains Player {@link BiomeTile#setPlayerID}
     */
    public void placeUnverified(TilePlacement tp, BiomeTile tile){
        Coords toBePlacedAt = new Coords(tp);
        grid.put(toBePlacedAt, tile.turn(tp.getRotation()));
        createSpace(toBePlacedAt);
    }

    /**
     * <P>checks if given TilePlacement and Tile represent a valid move and Tile could be placed at that location</P>
     * valid moves are determined by:
     * if and if only {@link Board#validBlank(Coords)} && {@link Board#matchingEdges(Coords, Tile)} && {@link Board#matchingPlayerID(Coords, BiomeTile)} return true
     * uses {@link BiomeTile#turn} to rotate {@link BiomeTile}
     * @param tp {@link TilePlacement} given by Player, provides location and rotation
     * @param tile {@link BiomeTile} to be placed, (not-rotated) tile
     * @return true if move is valid
     */
    public boolean verifyMove(TilePlacement tp, BiomeTile tile) {
        // skip
        if (tp == null) {
            return noLegalMove(tile);
        // no skip
        } else {
            BiomeTile turnedTile = tile.turn(tp.getRotation());
            Coords toBeValidated = new Coords(tp);

            return validBlank(toBeValidated) &&
                    matchingEdges(toBeValidated, turnedTile) &&
                    matchingPlayerID(toBeValidated, turnedTile);
        }
    }

    /**
     * determines if at the Coordinates, where a tile should be placed is a blank tile, so that a placement is legal
     * blank tile means :
     * in {@link Board#grid} is already a key (coordinates)  but no value associated with that key (value == null)
     * see {@link Board#createSpace(Coords)} for information how blank tiles are created
     * @param tryToPlaceAt the {@link Coords} where a placement is attempted
     * @return true if there is a Key with no associated value, false if there is a value or no key at all
     */
    private boolean validBlank (Coords tryToPlaceAt){
        if(grid.containsKey(tryToPlaceAt)){
            return grid.get(tryToPlaceAt) == null;
        }
        return false;
    }

    /**
     * determines if WATER or TRAINTRACKS edges on neighbouring tiles match
     * @param toMatchAt the Coordinates where the Tile should be placed
     * @param toBePlaced the Tile, which should be placed
     * @return true if the edges are matching, else false
     */
    private boolean matchingEdges (Coords toMatchAt, Tile toBePlaced){
        for (int i = 0; i < 6; i++) {
            if(toBePlaced.getEdge(i)==Biome.WATER){
                // simply no edge exists in this direction
                if(grid.get(searchKey(toMatchAt.getQ()+nEID[i][0], toMatchAt.getR()+nEID[i][1], toMatchAt.getS()+nEID[i][2])) == null) {
                    continue;
                } // else check edge
                if(! (grid.get(searchKey(toMatchAt.getQ()+nEID[i][0], toMatchAt.getR()+nEID[i][1], toMatchAt.getS()+nEID[i][2])).getEdge(((i+3)%6)) == Biome.WATER)) {
                    return false;
                }
            } else if(toBePlaced.getEdge(i)==Biome.TRAINTRACKS){
                if(grid.get(searchKey(toMatchAt.getQ()+nEID[i][0], toMatchAt.getR()+nEID[i][1], toMatchAt.getS()+nEID[i][2])) == null) {
                    continue;
                }
                if(! (grid.get(searchKey(toMatchAt.getQ()+nEID[i][0], toMatchAt.getR()+nEID[i][1], toMatchAt.getS()+nEID[i][2])).getEdge(((i+3)%6)) == Biome.TRAINTRACKS)) {
                    return false;
                }
            }

            if(!(toBePlaced.getEdge(i)==Biome.WATER)) {
                // simply no edge exists in this direction
                if (grid.get(searchKey(toMatchAt.getQ() + nEID[i][0], toMatchAt.getR() + nEID[i][1], toMatchAt.getS() + nEID[i][2])) == null) {
                    continue;
                } // else check edge
                if ((grid.get(searchKey(toMatchAt.getQ() + nEID[i][0], toMatchAt.getR() + nEID[i][1], toMatchAt.getS() + nEID[i][2])).getEdge(((i + 3) % 6)) == Biome.WATER)) {
                    return false;
                }
            }

            if(!(toBePlaced.getEdge(i)==Biome.TRAINTRACKS)) {
                if (grid.get(searchKey(toMatchAt.getQ() + nEID[i][0], toMatchAt.getR() + nEID[i][1], toMatchAt.getS() + nEID[i][2])) == null) {
                    continue;
                }
                if ((grid.get(searchKey(toMatchAt.getQ() + nEID[i][0], toMatchAt.getR() + nEID[i][1], toMatchAt.getS() + nEID[i][2])).getEdge(((i + 3) % 6)) == Biome.TRAINTRACKS)) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * if any direction from Coords where the tile should be placed match the player id, placing the tile returns true
     * @param toMatchAt where to place the tile
     * @param toBePlaced the BiomeTile to be placed
     * @return true if at least one neighbouring tile, was placed by the player who tries to place this tile
     */
    private boolean matchingPlayerID(Coords toMatchAt, BiomeTile toBePlaced ) {
        for (int i = 0; i < 6; i++) {
            // blank tile can't be evaluated
            if (grid.get(searchKey(toMatchAt.getQ()+nEID[i][0], toMatchAt.getR()+nEID[i][1], toMatchAt.getS()+nEID[i][2])) == null){
                continue;
            }
            // owner playerID = 0 (everyone owns, always permitted)
            if(grid.get(searchKey(toMatchAt.getQ()+nEID[i][0], toMatchAt.getR()+nEID[i][1], toMatchAt.getS()+nEID[i][2])).getPlayerID() == 0){
                return true;
            }
            // owner matches the player trying to place the tile
            if(grid.get(searchKey(toMatchAt.getQ()+nEID[i][0], toMatchAt.getR()+nEID[i][1], toMatchAt.getS()+nEID[i][2])).getPlayerID() == toBePlaced.getPlayerID()){
                return true;
            }
        }
        return false;
    }

    /**
     * provides a list of all legal moves for a given Tile of a player
     * @see Board#verifyMove(TilePlacement, BiomeTile)
     * @param tile the current {@link BiomeTile} to be placed, MUST CONTAIN PLAYERID
     * @return List of {@link TilePlacement} all legal moves
     */
    public Set<TilePlacement> provideLegalMoves(BiomeTile tile){

        Set<TilePlacement> allLegalMoves = new HashSet<>();
        for (Coords blankTile:
                getBlanks()) {
            for (int i = 0; i < 6; i++) {
                TilePlacement tp = new TilePlacement(blankTile.getOddqRow(), blankTile.getOddqColumn(), i);
                if(verifyMove(tp, tile)){
                    allLegalMoves.add(tp);
                }
            }
        }
        return allLegalMoves;
    }


    /**
     * determines if for a given BiomeTile (consisting a PlayerID) is any legal move possible
     * @see Board#verifyMove(TilePlacement, BiomeTile)
     * @see Board#getBlanks()
     * @param tile the {@link BiomeTile} consisting PlayerID
     * @return false if there is at least one legal move, else true
     */
    public boolean noLegalMove(BiomeTile tile){
        for (Coords blankTile:
                getBlanks()) {
            for (int i = 0; i < 6; i++) {
                if(verifyMove(new TilePlacement(blankTile.getOddqRow(), blankTile.getOddqColumn(), i), tile)){
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * provides a list of Coords of all current blank tiles
     * blank tile means :
     *      in {@link Board#grid} is already a key (coordinates)  but no value associated with that key (value == null)
     *      see {@link Board#createSpace(Coords)} for information how blank tiles are created
     * @return all current blank tiles
     */
    public List<Coords> getBlanks(){
        List<Coords> blanks = new ArrayList<>();
        for(Entry<Coords, BiomeTile> val : grid.entrySet()) {
            if(val.getValue()==null){
                blanks.add(val.getKey());
            }
        }
        return blanks;
    }

    /**
     * this method supplies needed "blank tiles" when a tile is placed
     * blank tiles are Coords without associated BiomeTile in the grid
     * therefore blank is represented with null reference
     * @param pos the position to createSpace around
     */
    private void createSpace(Coords pos){
        for (int i = 0; i < 6; i++) {
            if(grid.get(searchKey(pos.getQ()+nEID[i][0], pos.getR()+nEID[i][1], pos.getS()+nEID[i][2])) == null){
                grid.put(new Coords(pos.getQ()+nEID[i][0], pos.getR()+nEID[i][1], pos.getS()+nEID[i][2]), null);
            }
        }
    }

    /**
     * this method searches for a reference for a Key in the {@link Board#grid} and returns it
     * @param q the column in cube coordinates
     * @param r the row in cube coordinates
     * @param s third coordinate in cube coordinates
     * @return reference to Key for this variables
     */
    private Coords searchKey (int q, int r , int s){
        for(Entry<Coords, BiomeTile> val : grid.entrySet()) {
            if(val.getKey().getQ()==q && val.getKey().getR() == r && val.getKey().getS() == s){
                return val.getKey();
            }
        }
        return null;
    }


    /**
     * returns current grid of BiomeTiles and Coords
     * @return  Map<Coords, BiomeTile> grid
     */
    public Map<Coords, BiomeTile> getGrid() {
        return grid;
    }
}