package doerfer.hexbiom.board;


import doerfer.preset.TilePlacement;

import java.util.Objects;

/**
 * <P>Coords class row and column of odd-q layout and creates obj in cube coords</P>
 * <P>intention is to create objs which can be keys in a map and therefore are comparable with equals</P>
 * {@link TilePlacement TilePlacement} is converted to a unique obj for mapping
 * rotation is handled with {@link doerfer.hexbiom.board.BiomeTile#turn}
 */
public class Coords {
    final int q; //column in cube coords
    final int r; //row in cube coords
    final int s; //in cube coords

    /**
     * Constructor for Coords obj (Cube Coords)
     * @param q Q-Axis value as int
     * @param r R-Axis value as int
     * @param s S-Axis value as int
     */
    // q+r+s=0
    public Coords(int q, int r, int s){
        this.q=q;
        this.r=r;
        this.s=s;
    }

    /**
     * Constructor for Coords obj (Cube Coords) from oddq Layout
     * ref: <a href="https://www.redblobgames.com/grids/hexagons/#conversions">...</a>
     * @param tp {@link TilePlacement TilePlacement} in oddq layout (row/col may be negative)
     */
    public Coords(TilePlacement tp){
        // umrechnung von oddq zu cube
        q = tp.getColumn();
        r = tp.getRow() - (tp.getColumn()-(tp.getColumn()&1))/2; //a&1 (bitwise and) instead a%2 to dected even(0), odd(1) bcuz works with negative too
        s = -q-r;

    }

    /**
     * Indicates whether some other object is "equal to" this one
     * @param o Object to compare to
     * @return true if this object has the same q-/ r-/ and s-coordinates as the obj argument or is the argument; false otherwise
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Coords coords = (Coords) o;
        return q == coords.q && r == coords.r && s == coords.s;
    }

    /**
     * Returns a hash code value for the object as Integer using Object.hash() for q, r and s
     * @return a hash code value for this object
     */
    @Override
    public int hashCode() {
        return Objects.hash(q, r, s);
    }

    /**
     * returns q-coordinate of given Coords Obj
     * @return value of q as int
     */
    public int getQ() {
        return q;
    }

    /**
     * returns r-coordinate of given Coords Obj
     * @return value of r as int
     */
    public int getR() {
        return r;
    }

    /**
     * returns s-coordinate of given Coords Obj
     * @return value of s as int
     */
    public int getS() {
        return s;
    }

    /**
     * returns row of given Coord obj converted to OddQ-layout
     * @return row as integer
     */
    public int getOddqRow(){
        return r + (q - (q &1)) / 2;
    }

    /**
     * returns column of given Coord obj converted to OddQ-layout
     * @return column as integer
     */
    public int getOddqColumn(){
        return q;
    }

    /**
     * Returns a string representation of the Coords object
     * @return a string representation
     */
    @Override
    public String toString() {
        return "Coords{" +
                "q=" + q +
                ", r=" + r +
                ", s=" + s +
                '}';
    }
}
