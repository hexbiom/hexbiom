package doerfer.hexbiom;

import  doerfer.preset.PlayerType;
import  doerfer.hexbiom.player.CheatException;

public class CheatExceptionBy extends CheatException {
    private String     pName;       public String     getPName() { return pName; }
    private PlayerType pType;       public PlayerType getPType() { return pType; }
    private String     situation;   public String     getSituation() { return situation; }

    public CheatExceptionBy(String reason, String playerName, PlayerType playerType, String when) {
        this(new CheatException(reason), playerName, playerType, when);
    }

    public CheatExceptionBy(CheatException e, String playerName, PlayerType playerType, String when) {
        super(e.getReason());
        situation = when;
        if (situation == null || situation.trim() == "") situation = "(no sitation given)";

        pName = playerName;
        pType = playerType;
    }

    @Override
    public String getMessage() {
        return pType.toString()+" "+pName+" complains on "+situation+":\n    "+reason;
    }
}
