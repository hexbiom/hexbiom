package doerfer.hexbiom.player;

import doerfer.hexbiom.board.BiomeTile;
import doerfer.hexbiom.score.*;
import doerfer.preset.*;

import java.util.*;

/**
 * Abstract Class which implements the {@link Player} interface and extends {@link Attender}
 * used to implement players of different types.
 */
public abstract class AbstractPlayer extends Attender implements Player {
    protected long generatedSeed;
    protected RNG randomNumbers;
    protected final String name;
    protected boolean initialized = false;
    protected int playerID;
    private List<BiomeTile> uncovered;
    private GameConfiguration gameConfig;

    /**
     * Constructor for player using a name
     * @param name of player
     */
    protected AbstractPlayer(String name) {
        super();
        this.name = name;
    }

    /**
     * initializes player if boolean {@link AbstractPlayer#initialized} is false
     * @param gameConfiguration config to generate from
     * @param playerId of the player
     * @throws CheatException if player was already initialized
     */
    @Override
    public void init(GameConfiguration gameConfiguration, int playerId) throws CheatException {
        //throws CheatException if construction of board produces IllegalPreplacedTilesException
        init_(gameConfiguration);

        if (initialized) throw new CheatException("player was already initialized");
        initialized = true;

        randomNumbers = new RNG();

        generatedSeed = randomNumbers.getSeed();

        this.playerID = playerId;

        uncovered = new ArrayList<>();

        gameConfig = gameConfiguration;
    }

    /**
     * notifies player about the last new uncovered {@link Tile}
     * adds tile to the {@link AbstractPlayer#uncovered} List to be verified at the end of the game
     * @param tp tile placement
     * @throws CheatException if someone cheated
     */
    @Override
    public void notifyTilePlacement(TilePlacement tp) throws CheatException {
        if (queue.isEmpty()) throw new CheatException("no tiles left to place!");
        if (!board.verifyMove(tp, queue.peek()))
            throw new CheatException(tp == null
                ? "Illegal skip - there were possible moves!"
                : "The tile placement is against the rules!"
            );

        notifyTilePlacement_(tp);
    }

    /**
     * notifies player about the last new uncovered {@link Tile}
     * adds tile to the {@link AbstractPlayer#uncovered} List to be verified at the end of the game
     * @param tile to place
     * @throws CheatException if no tiles left to uncover
     */
    @Override
    public void notifyNewUncoveredTile(Tile tile) throws CheatException {
        notifyNewUncoveredTile_(tile);
        uncovered.add((BiomeTile) tile);

        if (uncoversLeft < 0)
            throw new CheatException("no tiles left to uncover!");
    }

    /**
     * return the current score of the player using {@link Score}
     * @return score of the player
     */
    @Override
    public int getScore() {
        Score scoring = new Score(board);
        return scoring.getAllScores(numPlayers).get(playerID);
    }

    /**
     * verfies if game was played correctly from this players perspective
     * @param seeds List of player seeds provided by {@link doerfer.hexbiom.Main}
     * @param scores List of player scores provided by {@link doerfer.hexbiom.Main}
     * @throws CheatException if game could not be verified
     */
    @Override
    public void verifyGame(List<Long> seeds, List<Integer> scores) throws CheatException {

        //same scores
        for(int player = 1; player <= (numPlayers); player++) {
            Score scoring = new Score(board);
            if (!(Objects.equals(scoring.getAllScores(numPlayers).get(player), scores.get(player)))) {
                throw new CheatException("Game can't be verified, scores not matching");
            }

        }

        //same seed
        if(generatedSeed != seeds.get(playerID-1)) {
            throw new CheatException("Game can't be verified, player's seed not matching");
        }

        //same uncovered tiles
        long playerSeeds = 0;
        for (long seed: seeds) {
            playerSeeds ^= seed;
        }

        TileGenerator tileGen = new TileGenerator(gameConfig);

        for (BiomeTile tile:uncovered) {
            if(tile.compareBiomes(new BiomeTile(tileGen.generateTile(playerSeeds)))) {
                throw new CheatException("Game can't be verified, generated tiles not matching");
            }
        }
    }


    /**
     * requests the next randomNumber from the RNG instance of the player
     * @return next random number
     */
    @Override public long requestNextRandomNumber() { return randomNumbers.next(); }

    /**
     * request the initially generated seed
     * @return random number seed
     */
    @Override public long requestRandomNumberSeed() { return generatedSeed; }

    /**
     * returns the player name
     * @return player name as string
     */
    @Override
    public String  getName      () { return this.name  ; }

    /**
     * returns the player's playerID
     * @return playerID as int
     */
    public int     getPlayerID  () { return playerID   ; }
}
