package doerfer.hexbiom.player;

public class CheatException extends Exception {
    protected final String reason;
    public String getReason() { return reason; }

    public CheatException(String reason) {
        super((reason == null || reason.trim().equals("")) ?"(no reason given)" : reason);
        this.reason = reason;
    }
}
