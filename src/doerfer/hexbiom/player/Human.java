package doerfer.hexbiom.player;

import doerfer.hexbiom.gui.Interface;
import doerfer.preset.*;

/**
 * extends {@link AbstractPlayer}
 * player which is able to click on screen to request a tile placement
 */
public class Human extends AbstractPlayer {

    private final Interface gui;

    /**
     * Constructor for a Human {@link AbstractPlayer}
     * @param name of the player
     * @param gui the {@link Interface} responsible to handle this human's input
     */
    public Human(String name, Interface gui) {
        super(name);
        this.gui = gui;
    }

    /**
     * requests TilePlacement from player.
     * returns null if move must be skipped.
     * gets valid tile placements for gui representation and returns {@link TilePlacement} send by mouse click
     * @return {@link TilePlacement} send by mouse click in gui
     */
    @Override
    public TilePlacement requestTilePlacement()  {
        //skip needed
        if(board.noLegalMove(queue.peek())) return null;
        //move
        else {
            gui.setValidTilePlacements(board.provideLegalMoves(queue.peek()));
            return gui.requestTilePlacement();
        }

    }
}
