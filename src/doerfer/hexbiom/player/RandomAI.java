package doerfer.hexbiom.player;

import doerfer.hexbiom.board.BiomeTile;
import doerfer.preset.*;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * extends {@link AbstractPlayer}
 * implements a simple bot which makes move based on chance
 * uses {@link doerfer.hexbiom.board.Board#noLegalMove(BiomeTile)} method and chooses random entry as next valid move.
 */
public class RandomAI extends AbstractPlayer {

    /**
     * Constructor for Human obj.
     * @param name of the human player
     */
    public RandomAI(String name) {
        super(name);
    }

    /**
     * uses the provided random {@link TilePlacement} to make a move.
     * returns the {@link TilePlacement} or null if no valid moves are possible
     * @return tp the {@link TilePlacement} or null
     * @throws CheatException if wrongPlayer tried to take a turn
     */
    @Override
    public TilePlacement requestTilePlacement() throws CheatException {
        BiomeTile tileToPlace = queue.peek();

        if (tileToPlace == null) throw new CheatException("no tile to place, but turn requested");
        if (tileToPlace.getPlayerID() != playerID)
            throw new CheatException("wrong player, other player's turn!");

        if (board.noLegalMove(tileToPlace)) return null;

        // make a list of the legal move to choose a random index
        List<TilePlacement> moves = new ArrayList<>(board.provideLegalMoves(tileToPlace));

        return moves.get(ThreadLocalRandom.current().nextInt(0, moves.size()));
    }
}
