# Setup

Just run `npm install` in this repo, the rest happens automatically, this is just for refference how i set up the pre-commit-hook for prettier.

## prettier (npm)

[https://prettier.io/docs/en/install.html]

```
npm install --save-dev --save-exact prettier
# echo {}> .prettierrc.json
```

### java plugin

[https://github.com/jhipster/prettier-java]

```
npm install prettier-plugin-java --save-dev
```

### xml plugin

[https://github.com/prettier/plugin-xml]

```
npm install --save-dev prettier @prettier/plugin-xml
```

### pre-commit hook

[https://prettier.io/docs/en/install.html#git-hooks]

```
npm install --save-dev husky lint-staged
npx husky install
npm set-script prepare "husky install"
npx husky add .husky/pre-commit "npx lint-staged"
```
