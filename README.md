# HEXBIOM

JavaDoc at GitLab pages: [hexbiom.pages.gwdg.de/hexbiom](https://hexbiom.pages.gwdg.de/hexbiom/)

Explanation is [here](https://gitlab.gwdg.de/app/2022ss/doerfer-preset/-/issues/6#note_673316).


## Notes on encountered problems

May be documented in `notes/<dude>.md` with headings beginning with `?<id>` 
and refered to with `<dude>?<id>` such that your fellows can read what were you thougts
without cluttering source code tooo much :)

