﻿# Setup-Guide

Das Projekt befindet sich in einem .tar-Archiv, das wie bekannt entpackt werden kann. 
Das Kompilieren und Ausführen des Projekts kann mit der im Verzeichnis befindlichen Ant-Datei build.xml erfolgen. Folgende Targets stehen dabei zur Verfügung:

#### Ant
| Ant-Target| Beschreibung und How-to-Use  |
|--|--|
| clean| wird benutzt, um das Projekt-Verzeichnis aufzuräumen, indem Build- und Docs-Verzeichnis gelöscht wird.  |
|compile| Kompiliert das Projekt im Source-Verzeichnis in das Build-Verzeichnis.  |
|jar | ist abhängig vom compile target und erzeugt aus dem vorher kompilierten Programm eine Jar-Datei mit namens hexbiom.jar. Diese enthält auch die benötigten Dependencies aus dem lib-Verzeichnis.|
|run | ist abhängig vom jar target und führt die erzeugte jar-Datei aus. Benötigt dabei eine Eingabe über die Kommandozeile. Beim Ausführen wird die Eingabe in einem Prompt abgefragt.   |
|default |ist abhängig vom target jar und compile und führt eine erzeugte Jar-Datei mit einer festen Konfiguration aus (Verwendete Konfiguration: "*-c config/Sample.cfg -d 2000 -pt HUMAN RANDOM_AI RANDOM_AI RANDOM_AI -pc red yellow green blue -pn Player1 bot1 bot2 bot3* ").   |
|doc| erzeugt eine JavaDoc-Dokumentation aus den im src-Verzeichnis befindlichen java-Dateien|

#### Flags
Das Projekt kann ohne weitere Eingaben mit dem debug-Target gestartet werden. Hierbei wird die sample.cfg verwendet und es werden ein *HUMAN*- und drei *RANDOM_AI*-Spieler geladen.
Für individuelle Einstellungen kann die sample.cfg wie in den Anforderungen vorgegeben, verändert werden. Außerdem können über die Kommandozeile folgende flags übergeben werden:
|Flag| Beschreibung und How-to-Use |
|--|--|
| -c,--config &lt;FILE&gt; | muss verwendet werden, damit das Projekt startet. Lädt die angegebene Config-Datei |
|-d,--delay &lt;DELAY&gt; |fügt eine Verzögerung in Millisekunden ein, nachdem ein Bot einen Zug gemacht hat. |
|-pc,--playerColors &lt;COLOR ...&gt;| definiert die angezeigten Farben der/des Spieler/s|
|-pn,--playerNames &lt;NAME ...&gt;| definiert die angezeigten Namen der/des Spieler/s|
| -pt,--playerTypes &lt;TYPE ...&gt;|definiert welcher Spieler-Typ verwendet werden soll (Verfügbare Typen: *HUMAN*, *RANDOM_AI*)|

#### Verfübare Shortcuts:
| Taste| Funktion  |
|--|--|
| q | aktuelle Karte nach links rotieren|
| e | aktuelle Karte nach links rotieren|
| + | Zoom in |
| - | Zoom out|
|0|Zoom und camera position reset|
|Pfeiltaste links| Kamerabewegung nach links|
|Pfeiltaste aufwärts|Kamerabewegung nach oben|
|Pfeiltaste abwärts|Kamerabewegung nach unten|
|Pfeiltaste rechts| Kamerabewegung nach rechts|
|x| wird nach dem Ende des Spiels zum Schließen des Programms verwendet|