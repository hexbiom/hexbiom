# Problems encountered and functional style in java

## combersome declaration of lambdas

separation of concerns is good? small functions are good? readable code is good? Then why 

```java
Function<Integer, String> bla = arg -> foo.apply(bar(arg));
```
while this is possible: 

```haskell
bla = foo . bar
```

even worse, because of half-hearted support for lambdas and none for local functions one is forced to make classemthod out of most things:

```java
private Integer bla(String arg) {
    return foo(bar(arg));
}  
``` 

??

## Exceptions everywhere, ADTs missing

everything raises exceptions, is it so hard to implement structured controll flow? this forces me to clutter everything wiht try-catch, which really 
heavily hardens understandability of code (am I not alone?) od forces me to implement own solutions which others then need to follow... 

I even cant propperly because algebraic data types are impossible(?) to implement in java? Are they that hard? 
Has everyone surely understood them enough to agree upon their harmfullness? then please explain it to me!

How is one supposed to write understandable and maintainable code in a productive and assurable way like this?


## Lambdas and Exceptions 

[dont go well together](https://www.baeldung.com/java-lambda-exceptions) which is a shame, because as allready mentioned, Excptions are EVERYWHERE.




## Conclusion

I have some intuition how ideomatic functional code looks, and am quite convinced about its superiority. 
These techniques are hard to apply in java. I feel i must trade clutter for clutter. 
Is there ideomatic java that is as concise, understandable, predictable or for any other reason as good as ideomatic Haskell?

Why then use java?! At least Rust or Scala plz. Have you any reasons? 
