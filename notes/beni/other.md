

## (Auto)boxing and primitive types

- primitives are mostly much faster

- For functional interfaces thus use the provided primitive variant (`DoubleConsumer` instead of `Consumer<Double>`).



## default parameters

their missing leads to extrem inflexibility of methods only to be avoided by massive overloading boilderplate or Paramter Objects (also boilerplate)

## precision for animations

though overkill


## concurrent collections

https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/concurrent/BlockingQueue.html

use the appropriate blocking methods, else stuff might not happen!


## synchronized

using synchronized in varios mathods of the animation part caused randomly ignoring commands!

=> if synchronized do it right ? how is right?