
Logik um das Spiel mitzuschreiben und zu bewerten, muss für alle Spieler 
implementiert sein, daher erben sie alle von einer abstract class, 
die einen aufmerksamen Zuschauer darstellt (hält Spielbrett, Liste von Spielern und 
aufgedeckten Tiles, ...). Unser GUI (die GameView implementiert) 
erbt auch davon und nutzt diese Funktionalität mit. 
Das erschien uns als die sauberste Lösung, um nicht ähnliche und zusätzliche
Funktionalität in Main (unserem Hauptprogram) zu hinterlegen, und das Hauptprogramm
auf Kernfunktionalität zu fokussieren (den Austausch zwischen allen Spielern und 
den fundamentalen Programmablauf koordinieren). Das führt dazu, dass ein Teil der 
Methoden aus GameView von uns nicht benutzt werden _muss_ - deren Funktionalitäten
werden stattdessen von den überschriebenen Zuschauer-Methoden 
in Reaktion auf Spielereignisse
automatisch ausgelöst (`notifyTilePlacement` impliziert z.B. dass der aktive Spieler
eins weiter geht, was `setActivePlayer` redundant macht). 
Die Methoden von GameView, die wir wohl zumindest 
nicht explizit vom Hauptprogramm aufrufen werden (teilweise aber innerhalb des GUI benutzen werden) sind daher mit Begründung:
​
- `placeTile` - ersetzt / ausgelöst durch `notifyTilePlacement` um gemeinsame Funktionalität mit Zuschauern zu nutzen.

- `setActivePlayer`, `setPlayerSkipped`, `setTileleft`, `setValidTilePlacements` - ersetzt / ausgelöst im Rahmen von `notifyTilePlacement`.

- `setUncoveredTiles` - ersetzt / ausgelöst durch `notifyNewUncoveredTile` um gemeinsame Funktionalität mit Zuschauern zu nutzen.

Unabhängig davon wollen wir Informationen für die graphische Darstellung der Spieler
bei Konstruktion der GUI aus den Spieleinstellungen beziehen und die Übergabe von echten Spielern an die GUI vermeiden
(diese werden ausschließlich im Hauptprogramm verwaltet), sodass wir `setPlayers` nicht benutzen. 